import json
import argparse
parser = argparse.ArgumentParser(description='Converts a list of words to a json list.')
parser.add_argument('file', metavar='file', type=str,
                    help='The name of the file to be converted')
words = []
args = parser.parse_args()
print(args)
with open(args.file , 'r') as f:
	for line in f:
		words.append(line.strip('\n').lower())
	print(json.dumps(words))