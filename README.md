This is a board game server built on top of [boardgame.io](https://boardgame.io).
We have optimized the lobby experience for local/zoom multiplayer, á la jackbox.tv.

### Demo
Live at [https://plep.gitlab.io/games](https://plep.gitlab.io/games).

#### Server

To setup the server:

```bash
git clone https://github.com/peelin/boardgame.io.git
cd boardgame.io 
npm install
npm run prepack
cd ..
git clone https://github.com/peelin/react-sortable-hoc
git clone https://gitlab.com/plep/games.git
cd games
npm install
npm link ../boardgame.io

npm run server-dev-start
```


#### Client
Make sure to update src/config.js to point to the server.


```
image: node:12.3.1 # can be upgraded, depending on your node version used

pages:
  cache:
    paths:
      - node_modules/

  stage: deploy
  script:
    - npm cache verify
    - npm install https://github.com/peelin/boardgame.io#noScript
    - npm install https://github.com/peelin/react-sortable-hoc#fsfix
    - npm install
    - CI=false GENERATE_SOURCEMAP=false npm run build
    - rm -rf public
    - mv build public
    - gzip -k -6 $(find public -type f)
  artifacts:
    paths:
      - public # mandatory, other folder won't work
  only:
    - master # or dev, the branch you want to publish
```
