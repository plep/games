export const ORIG = [
  "hollywood",
  "screen",
  "play",
  "marble",
  "dinosaur",
  "cat",
  "pitch",
  "bond",
  "greece",
  "deck",
  "spike",
  "center",
  "vacuum",
  "unicorn",
  "undertaker",
  "sock",
  "loch ness",
  "horse",
  "berlin",
  "platypus",
  "port",
  "chest",
  "box",
  "compound",
  "ship",
  "watch",
  "space",
  "flute",
  "tower",
  "death",
  "foot",
  "torch",
  "arm",
  "figure",
  "mine",
  "suit",
  "crane",
  "beijing",
  "mass",
  "microscope",
  "engine",
  "china",
  "straw",
  "pants",
  "europe",
  "boot",
  "princess",
  "link",
  "luck",
  "olive",
  "palm",
  "teacher",
  "thumb",
  "octopus",
  "hood",
  "tie",
  "doctor",
  "wake",
  "cricket",
  "millionaire",
  "spring",
  "match",
  "diamond",
  "centaur",
  "march",
  "roulette",
  "dog",
  "cross",
  "wave",
  "duck",
  "wind",
  "spot",
  "skyscraper",
  "paper",
  "apple",
  "oil",
  "cook",
  "fly",
  "cast",
  "bear",
  "pin",
  "thief",
  "trunk",
  "america",
  "novel",
  "cell",
  "bow",
  "model",
  "knife",
  "knight",
  "tube",
  "block",
  "comic",
  "fish",
  "bridge",
  "moon",
  "part",
  "aztec",
  "smuggler",
  "train",
  "embassy",
  "pupil",
  "scuba diver",
  "ice",
  "tap",
  "code",
  "shoe",
  "server",
  "club",
  "row",
  "pyramid",
  "bug",
  "penguin",
  "pound",
  "himalayas",
  "czech",
  "rome",
  "eye",
  "board",
  "bed",
  "lemon",
  "nurse",
  "drop",
  "track",
  "bank",
  "germany",
  "worm",
  "ray",
  "capital",
  "strike",
  "war",
  "concert",
  "honey",
  "canada",
  "buck",
  "snowman",
  "beat",
  "jam",
  "copper",
  "beach",
  "date",
  "stream",
  "missile",
  "scale",
  "band",
  "angel",
  "press",
  "berry",
  "card",
  "check",
  "draft",
  "head",
  "lap",
  "orange",
  "ice cream",
  "film",
  "washer",
  "pool",
  "shark",
  "van",
  "string",
  "calf",
  "hawk",
  "eagle",
  "needle",
  "forest",
  "dragon",
  "key",
  "belt",
  "cap",
  "tablet",
  "australia",
  "green",
  "egypt",
  "line",
  "lawyer",
  "witch",
  "parachute",
  "crash",
  "gold",
  "note",
  "lion",
  "plastic",
  "web",
  "ambulance",
  "hospital",
  "spell",
  "lock",
  "water",
  "london",
  "casino",
  "cycle",
  "bar",
  "cliff",
  "round",
  "bomb",
  "giant",
  "hand",
  "ninja",
  "rose",
  "well",
  "fair",
  "tooth",
  "staff",
  "bill",
  "shot",
  "king",
  "pan",
  "square",
  "buffalo",
  "scientist",
  "chick",
  "atlantis",
  "spy",
  "mail",
  "nut",
  "log",
  "pirate",
  "face",
  "stick",
  "disease",
  "yard",
  "mount",
  "slug",
  "dice",
  "lead",
  "hook",
  "carrot",
  "poison",
  "stock",
  "new york",
  "state",
  "bermuda",
  "park",
  "turkey",
  "chocolate",
  "trip",
  "racket",
  "bat",
  "jet",
  "shakespeare",
  "bolt",
  "switch",
  "wall",
  "soul",
  "ghost",
  "time",
  "dance",
  "amazon",
  "grace",
  "moscow",
  "pumpkin",
  "antarctica",
  "whip",
  "heart",
  "table",
  "ball",
  "fighter",
  "cold",
  "day",
  "court",
  "iron",
  "whale",
  "shadow",
  "contract",
  "mercury",
  "conductor",
  "seal",
  "car",
  "ring",
  "kid",
  "piano",
  "laser",
  "sound",
  "pole",
  "superhero",
  "revolution",
  "pit",
  "gas",
  "glass",
  "washington",
  "bark",
  "snow",
  "ivory",
  "pipe",
  "cover",
  "degree",
  "tokyo",
  "church",
  "pie",
  "point",
  "france",
  "mammoth",
  "cotton",
  "robin",
  "net",
  "bugle",
  "maple",
  "england",
  "field",
  "robot",
  "plot",
  "africa",
  "tag",
  "mouth",
  "kiwi",
  "mole",
  "school",
  "sink",
  "pistol",
  "opera",
  "mint",
  "root",
  "sub",
  "crown",
  "back",
  "plane",
  "mexico",
  "cloak",
  "circle",
  "bell",
  "leprechaun",
  "pheonix",
  "force",
  "boom",
  "fork",
  "alps",
  "post",
  "fence",
  "kangaroo",
  "mouse",
  "mug",
  "horseshoe",
  "scorpion",
  "agent",
  "helicopter",
  "hole",
  "organ",
  "jack",
  "charge",
  "drill",
  "glove",
  "paste",
  "fall",
  "fire",
  "spider",
  "spine",
  "soldier",
  "horn",
  "queen",
  "ham",
  "litter",
  "life",
  "temple",
  "rabbit",
  "button",
  "game",
  "star",
  "jupiter",
  "vet",
  "night",
  "air",
  "battery",
  "genius",
  "shop",
  "bottle",
  "stadium",
  "alien",
  "light",
  "triangle",
  "slip",
  "limousine",
  "pass",
  "theater",
  "plate",
  "satellite",
  "ketchup",
  "hotel",
  "tail",
  "tick",
  "ground",
  "police",
  "dwarf",
  "fan",
  "dress",
  "saturn",
  "grass",
  "brush",
  "chair",
  "rock",
  "pilot",
  "telescope",
  "file",
  "lab",
  "india",
  "ruler",
  "nail",
  "swing",
  "olympus",
  "change"
];

export const DUET = [
  "bride",
  "king arthur",
  "cowboy",
  "governor",
  "cable",
  "razor",
  "fog",
  "fever",
  "street",
  "texas",
  "napoleon",
  "earth",
  "cheese",
  "dentist",
  "wheel",
  "butterfly",
  "zombie",
  "joan of arc",
  "blacksmith",
  "clock",
  "columbus",
  "big bang",
  "pentagon",
  "army",
  "rice",
  "minute",
  "dollar",
  "shoot",
  "cow",
  "joker",
  "university",
  "camp",
  "mohawk",
  "anthem",
  "einstein",
  "brick",
  "crab",
  "polish",
  "captain",
  "letter",
  "director",
  "sister",
  "mountie",
  "whistle",
  "brass",
  "salad",
  "cleopatra",
  "bath",
  "goat",
  "lightning",
  "sherwood",
  "sphinx",
  "river",
  "saloon",
  "ink",
  "soup",
  "love",
  "lumberjack",
  "window",
  "collar",
  "ash",
  "leaf",
  "pitcher",
  "newton",
  "crystal",
  "tear",
  "skull",
  "paint",
  "axe",
  "wing",
  "scratch",
  "record",
  "sahara",
  "kitchen",
  "helmet",
  "smoke",
  "penny",
  "gum",
  "bee",
  "guitar",
  "nylon",
  "volcano",
  "judge",
  "mile",
  "tipi",
  "baby",
  "stethoscope",
  "dream",
  "sand",
  "dwarf",
  "nerve",
  "thunder",
  "musketeer",
  "glacier",
  "powder",
  "roll",
  "werewolf",
  "chain",
  "second",
  "bunk",
  "beard",
  "steam",
  "cloud",
  "valentine",
  "sticker",
  "snake",
  "battleship",
  "battle",
  "map",
  "virus",
  "tornado",
  "bicycle",
  "parrot",
  "hamburger",
  "sign",
  "noah",
  "wizard",
  "parade",
  "apron",
  "curry",
  "astronaut",
  "coast",
  "leather",
  "fiddle",
  "kiss",
  "bikini",
  "egg",
  "goldilocks",
  "popcorn",
  "ear",
  "caesar",
  "greenhouse",
  "dust",
  "slipper",
  "troll",
  "quack",
  "volume",
  "ski",
  "russia",
  "pearl",
  "pacific",
  "boil",
  "easter",
  "quarter",
  "microwave",
  "shower",
  "fuel",
  "earthquake",
  "crow",
  "magician",
  "beam",
  "desk",
  "saddle",
  "rat",
  "bacon",
  "rubber",
  "patient",
  "saw",
  "oasis",
  "venus",
  "bench",
  "rip",
  "st. patrick",
  "road",
  "chalk",
  "gymnast",
  "meter",
  "rodeo",
  "silk",
  "coffee",
  "jockey",
  "smoothie",
  "salsa",
  "sail",
  "wool",
  "tunnel",
  "garden",
  "smell",
  "door",
  "wood",
  "country",
  "elephant",
  "mark",
  "blind",
  "squash",
  "mother",
  "drawing",
  "rail",
  "miss",
  "purse",
  "igloo",
  "lip",
  "monkey",
  "bowler",
  "sweat",
  "driver",
  "delta",
  "salt",
  "sloth",
  "attic",
  "drum",
  "nose",
  "anchor",
  "shoulder",
  "spirit",
  "book",
  "kick",
  "boss",
  "rope",
  "avalanche",
  "sheet",
  "tuxedo",
  "shampoo",
  "frog",
  "milk",
  "gangster",
  "spurs",
  "step",
  "mummy",
  "kung fu",
  "wedding",
  "minotaur",
  "sleep",
  "eden",
  "boxer",
  "homer",
  "peach",
  "waitress",
  "computer",
  "tiger",
  "wagon",
  "stamp",
  "flood",
  "manicure",
  "swamp",
  "dressing",
  "radio",
  "banana",
  "pad",
  "sun",
  "sugar",
  "ram",
  "sack",
  "rainbow",
  "magazine",
  "story",
  "pig",
  "comet",
  "chip",
  "bean",
  "pop",
  "page",
  "groom",
  "paddle",
  "mirror",
  "sword",
  "tip",
  "shell",
  "peanut",
  "house",
  "hit",
  "brain",
  "santa",
  "halloween",
  "hide",
  "brazil",
  "cone",
  "knot",
  "wish",
  "rust",
  "ace",
  "bread",
  "sumo",
  "reindeer",
  "tea",
  "armor",
  "toast",
  "polo",
  "laundry",
  "bowl",
  "trick",
  "crusader",
  "farm",
  "mona lisa",
  "beer",
  "alaska",
  "jellyfish",
  "medic",
  "mustard",
  "hawaii",
  "doll",
  "lunch",
  "skates",
  "craft",
  "pew",
  "pen",
  "butter",
  "pillow",
  "castle",
  "scroll",
  "disk",
  "drone",
  "hammer",
  "floor",
  "jail",
  "break",
  "lace",
  "pocket",
  "tin",
  "flat",
  "cuckoo",
  "hair",
  "mud",
  "jeweler",
  "hercules",
  "blade",
  "mosquito",
  "pea",
  "iceland",
  "frost",
  "bass",
  "tutu",
  "barbecue",
  "balloon",
  "gear",
  "stable",
  "wheelchair",
  "storm",
  "shorts",
  "steel",
  "bucket",
  "dash",
  "walrus",
  "lemonade",
  "flag",
  "cave",
  "ant",
  "vampire",
  "tattoo",
  "ranch",
  "locust",
  "rifle",
  "viking",
  "blizzard",
  "makeup",
  "potter",
  "taste",
  "blues",
  "golf",
  "pine",
  "coach",
  "potato",
  "snap",
  "cake",
  "turtle",
  "christmas",
  "cherry",
  "moses",
  "team",
  "cane",
  "bulb",
  "biscuit",
  "spray",
  "pepper",
  "puppet",
  "mill",
  "maracas",
  "foam",
  "yellowstone",
  "ice age",
  "memory",
  "jumper",
  "comb",
  "glasses",
  "pizza",
  "soap",
  "big ben",
  "sled",
  "violet",
  "bubble",
  "spoon",
  "onion",
  "genie",
  "tank",
  "sherlock",
  "bay",
  "dryer",
  "brother",
  "mess",
  "ladder",
  "scarecrow",
  "notre dame",
  "kilt",
  "shed",
  "bonsai",
  "wonderland",
  "sling",
  "squirrel",
  "hose",
  "marathon"
];

export const UNDERCOVER = [
  "legend",
  "juice",
  "joystick",
  "johnson",
  "john",
  "jewels",
  "jerk",
  "hotel",
  "horny",
  "hooters",
  "high",
  "herb",
  "headlights",
  "headboard",
  "head",
  "hammer",
  "friction",
  "french",
  "freckles",
  "fluff",
  "flash",
  "fist",
  "fire",
  "fetish",
  "fecal",
  "feather",
  "fantasy",
  "drill",
  "drag",
  "down",
  "pickle",
  "period",
  "penis",
  "pee",
  "paddle",
  "package",
  "onion",
  "nylon",
  "nuts",
  "nude",
  "nipple",
  "needle",
  "necklace",
  "navel",
  "nail",
  "mug",
  "movie",
  "motorboat",
  "monkey",
  "mom",
  "moist",
  "mixer",
  "milk",
  "member",
  "meat",
  "martini",
  "lust",
  "love",
  "liquor",
  "lighter",
  "snake",
  "black",
  "bondage",
  "foreskin",
  "bar",
  "safe",
  "sack",
  "rug",
  "roach",
  "red",
  "rave",
  "queer",
  "queen",
  "queef",
  "purple",
  "pucker",
  "pub",
  "prostate",
  "prison",
  "pot",
  "porn",
  "pork",
  "pole",
  "poker",
  "player",
  "pitcher",
  "pipe",
  "pink",
  "pimp",
  "pig",
  "mouth",
  "sore",
  "mattress",
  "freak",
  "homerun",
  "screw",
  "flower",
  "strobe",
  "pound",
  "roof",
  "salad",
  "rack",
  "pie",
  "smegma",
  "shot",
  "snort",
  "baked",
  "bottom",
  "breast",
  "pinch",
  "sin",
  "diarrhea",
  "intern",
  "beer",
  "flesh",
  "skid",
  "facial",
  "bong",
  "daddy",
  "chest",
  "hooker",
  "pecker",
  "sheep",
  "roll",
  "regret",
  "gang",
  "peaches",
  "couple",
  "slut",
  "orgasm",
  "oyster",
  "melons",
  "gigolo",
  "latex",
  "cuffs",
  "rookie",
  "prick",
  "kitty",
  "lizard",
  "boxers",
  "missionary",
  "emission",
  "seed",
  "doggy",
  "lotion",
  "bust",
  "mesh",
  "nurse",
  "turd",
  "naked",
  "horse",
  "alcohol",
  "bitch",
  "hole",
  "balloon",
  "knob",
  "bender",
  "mushroom",
  "fatty",
  "mole",
  "brown",
  "ice",
  "hamster",
  "pillows",
  "carpet",
  "fish",
  "grass",
  "drunk",
  "cheek",
  "keg",
  "rectum",
  "cocktail",
  "cucumber",
  "noodle",
  "motel",
  "kinky",
  "joint",
  "poop",
  "line",
  "briefs",
  "dick",
  "dame",
  "condom",
  "clam",
  "cigarette",
  "chubby",
  "bush",
  "boob",
  "body",
  "behind",
  "beaver",
  "beans",
  "bacon",
  "ass",
  "animal",
  "blush",
  "bra",
  "apples",
  "banana",
  "candle",
  "beach",
  "crap",
  "boy",
  "catcher",
  "legs",
  "booze",
  "bear",
  "ashes",
  "bed",
  "chaps",
  "grandma",
  "eyes",
  "hurl",
  "bond",
  "bling",
  "dildo",
  "club",
  "choke",
  "bone",
  "rubber",
  "booty",
  "hump",
  "acid",
  "cougar",
  "clap",
  "hose",
  "couch",
  "furry",
  "experiment",
  "cowgirl",
  "grope",
  "bowl",
  "lick",
  "blonde",
  "eat",
  "gay",
  "escort",
  "commando",
  "gag",
  "hand",
  "bang",
  "jazz",
  "crabs",
  "douche",
  "finger",
  "manboobs",
  "crack",
  "loose",
  "bartender",
  "cream",
  "lobster",
  "knockers",
  "coyote",
  "burn",
  "cannons",
  "whiskey",
  "white",
  "wiener",
  "wine",
  "wood",
  "girl",
  "job",
  "butt",
  "brownie",
  "lube",
  "inch",
  "lingerie",
  "group",
  "hot",
  "gash",
  "film",
  "g-spot",
  "chains",
  "donkey",
  "lips",
  "cuddle",
  "bottle",
  "champagne",
  "gerbil",
  "balls",
  "knees",
  "cherry",
  "gangbang",
  "hell",
  "jugs",
  "touchdown",
  "toy",
  "train",
  "tramp",
  "trim",
  "trousers",
  "trunk",
  "tubesteak",
  "tuna",
  "twig",
  "udders",
  "uranus",
  "vasectomy",
  "vegas",
  "vein",
  "vibrator",
  "video",
  "vinyl",
  "virgin",
  "vodka",
  "vomit",
  "wad",
  "wang",
  "waste",
  "watch",
  "wax",
  "weed",
  "wench",
  "wet",
  "whip",
  "stool",
  "straight",
  "strap",
  "strip",
  "stripper",
  "stud",
  "swallow",
  "sweat",
  "swimmers",
  "taboo",
  "taco",
  "tail",
  "tap",
  "tavern",
  "teabag",
  "tease",
  "tent",
  "tequila",
  "threesome",
  "throat",
  "tickle",
  "tie",
  "tip",
  "tit",
  "tongue",
  "tool",
  "top",
  "torture",
  "touch",
  "orgy",
  "sauna",
  "sausage",
  "score",
  "secretary",
  "semen",
  "sex",
  "shaft",
  "shame",
  "share",
  "shave",
  "shower",
  "skank",
  "skirt",
  "smell",
  "smoke",
  "snatch",
  "sniff",
  "softballs",
  "solo",
  "spank",
  "speed",
  "sperm",
  "spoon",
  "spread",
  "squirt",
  "stalker",
  "steamy",
  "stiff",
  "stiletto",
  "stones",
  "cock",
  "chick",
  "bisexual",
  "blow",
  "cigar",
  "beef",
  "box",
  "biscuits",
  "dominate",
  "olive",
  "pussy",
  "log",
  "goose",
  "caboose",
  "coozie"
];
