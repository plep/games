// src/server.js
const VERSION = require("./version").VERSION;
const Server = require("boardgame.io/server").Server;
const CodeNames = require("./games/codenames/game").CodeNames;
const newGames = require("./games/gameCode").newGames;
const games = [CodeNames].concat(newGames);
const server = Server({ games });

server.run({ port: 8000 }, () =>
  console.log(`Server version ${VERSION} running on 8000...`)
);
