import React from "react";
import { PlayerSetupGrid } from "./playerSetupGrid";
import styles from "./setupScreens.module.css";
import globalStyles from "../globalStyles.module.css";
import { Config } from "../config";
import Mousetrap from "mousetrap";
import { CallToAction } from "./components";
import { Button } from "antd";

const serverURL = Config.serverURL;

interface HostSetupScreenProps {
  gameID: string;
  G: { order: number[] };
  moves: any;
  ctx: any;
  playerTitles: string[];
}

interface HostSetupScreenState {
  debugMode: boolean;
  players: PlayerInfo[];
  player_names: (string | undefined)[];
}

interface PlayerInfo {
  id: number;
  name: string | undefined;
}

export class HostSetupScreen extends React.Component<
  HostSetupScreenProps,
  HostSetupScreenState
> {
  autoUpdate: any;
  state = {
    debugMode: false,
    players: [], // a list of all players
    player_names: [] // if entry is null, it means slot is empty
  };

  swap = ({ oldIndex, newIndex }: { oldIndex: number; newIndex: number }) => {
    // Updates the mapping from roles to id
    const role_to_id = this.props.G.order;
    const newArray = [...role_to_id];
    newArray[oldIndex] = role_to_id[newIndex];
    newArray[newIndex] = role_to_id[oldIndex];
    this.props.moves.updateOrder(newArray);
  };

  async updatePlayerInfo() {
    try {
      const resp = await fetch(serverURL + "/games/" + this.props.gameID + "/");
      if (resp.status !== 200) {
        throw new Error("HTTP status " + resp.status);
      }
      const json = await resp.json();
      const players = json.players;
      this.setState({
        players: players,
        player_names: this._playerNames(players)
      });
    } catch (error) {
      throw new Error("failed to retrieve list of players (" + error + ")");
    }
  }

  get _numberOfPlayers() {
    // returns number of non-host players that have joined the game
    var numberOfPlayers = 0;
    for (let i = 1; i <= this.props.playerTitles.length; i++) {
      if (typeof this.state.player_names[i] !== "undefined") {
        numberOfPlayers += 1;
      }
    }
    return numberOfPlayers;
  }

  _playerNames(players: PlayerInfo[]) {
    // Return list of player names
    const player_names = [];
    for (let i = 0; i < this.props.playerTitles.length; i++) {
      player_names.push(players[i].name);
    }
    return player_names;
  }

  componentDidMount() {
    Mousetrap.bind("s", () => {
      this.setState({ debugMode: true });
    });
    this.autoUpdate = setInterval(() => {
      if (this.props.ctx.phase === "gameSetup") {
        this.updatePlayerInfo();
      }
    }, 1500);
  }

  componentWillUnmount() {
    Mousetrap.unbind("s");
    clearInterval(this.autoUpdate);
  }

  get canStartGame() {
    // Can start as soon as there is one spymaster
    return (
      this._numberOfPlayers >= this.props.playerTitles.length - 1 ||
      this.state.debugMode
    );
  }

  render() {
    const startGameButtonClasses = ["button", "is-primary", "is-large"];
    return (
      <div className={globalStyles.largeUiScreen + " " + globalStyles.noselect}>
        <CallToAction code={this.props.gameID} Config={Config} />
        <PlayerSetupGrid
          player_names={this.state.player_names}
          role_to_id={this.props.G.order}
          updateFunction={this.swap}
          playerTitles={this.props.playerTitles}
          maxPlayers={this.props.playerTitles.length}
        />
        <Button
          size={"large"}
          type={"primary"}
          className={startGameButtonClasses.join(" ")}
          onClick={() => this.props.moves.startGame()}
          // disabled={!this.canStartGame}
        >
          Start Game
        </Button>
      </div>
    );
  }
}
