import { SortableContainer, SortableElement } from "react-sortable-hoc";
import React from "react";
import styles from "./playerSetupGrid.module.css";

function shortenText(text: string, max: number) {
  // If text is longer than 'max' characters,
  // Returns shortened version
  // e.g. '3o2uiojfwfe' -> '3o2uio...'
  if (text.length > max) {
    return text.substring(0, max) + "...";
  } else {
    return text;
  }
}

const PlayerCard = SortableElement(
  ({ i, playerName }: { i: string; playerName: string }) => {
    const text = playerName ? playerName : `Waiting...`;
    const tagClasses = [styles.mtag];
    const tagStyle = {};
    if (playerName) {
      tagClasses.push(styles.occupied);
    }
    return (
      <div className={tagClasses.join(" ")} style={tagStyle}>
        <div className={styles.tagText}>{"" + shortenText(text, 14)}</div>
      </div>
    );
  }
);

const SortableRow = SortableContainer(
  ({ children, titles }: { children: any[]; titles: string[] }) => {
    return (
      <div className={styles.playersCard}>
        <table className={styles.playersTable}>
          <thead>
            <tr>
              {/* The first entry corresponds to the host */}
              {titles.map((title: string, idx: number) => (
                <th key={"title " + idx}>{title}</th>
              ))}
            </tr>
          </thead>
          <tbody>
            <tr id="playersCard">{children}</tr>
          </tbody>
        </table>
      </div>
    );
  }
);

interface IndexPair {
  oldIndex: number;
  newIndex: number;
}

interface PlayerSetupGridProps {
  updateFunction: (pair: IndexPair) => void;
  role_to_id: number[];
  player_names: string[];
  maxPlayers: number;
  playerTitles: string[];
}

export function PlayerSetupGrid(props: PlayerSetupGridProps) {

  return (
    <SortableRow
      helperClass={styles.gridHelper}
      onSortEnd={props.updateFunction}
      axis="x"
      titles={props.playerTitles}
    >
      {props.role_to_id.map((i: number, index: number) => (
        <td key={i}>
          <PlayerCard
            i={i.toString()}
            index={index}
            playerName={props.player_names[i]}
          />
        </td>
      ))}
    </SortableRow>
  );
}
