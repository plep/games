import React from "react";
import styles from "./components.module.css";
import QRious from "qrious";
import { Config } from "../config";
import globalStyles from "../globalStyles.module.css";
import { Typography } from "antd";
const { Text } = Typography;

interface CallToActionProps {
  Config: { gameURL_display: string; gameURL: string };
  code: string;
}

export function CallToAction(props: CallToActionProps) {
  const url = `${Config.gameURL}/#/play/${props.code}/`;
  return (
    <React.Fragment>
      <div className={globalStyles.instructions}>Join the game!</div>
      {/* <CopyBar enableToolTip={false} text={url} />
       */}
      <Text className={globalStyles.selectable} code copyable>
        {url}
      </Text>
      <QrCode url={url} />
    </React.Fragment>
  );
}

interface QrCodeProps {
  url: string;
}
export class QrCode extends React.Component<QrCodeProps, {}> {
  componentDidMount() {
    const element = document.getElementById("qr");
    new QRious({
      element: element,
      value: this.props.url,
      size: 256
    });
  }
  render() {
    return <canvas id="qr" className={styles.qrCode} />;
  }
}

export function ClientSetupScreen(props: any) {
  return (
    <React.Fragment>
      <div className={styles.clientSetupMsg}>Waiting for game to start...</div>
    </React.Fragment>
  );
}
