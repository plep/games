import React from "react";
import Clippy from "../icons/clippy.svg";
import styles from "./copyBar.module.css";
import { Button } from "antd";

export class CopyBar extends React.Component<
  { text: string; enableToolTip: boolean },
  { showCopyText: boolean }
> {
  componentDidMount() {
  }
  state = {
    showCopyText: false
  };

  flashText = () => {
    this.onText();
    setTimeout(this.offText, 2000);
  };

  onText = () => {
    this.setState({ showCopyText: true });
  };

  offText = () => {
    this.setState({ showCopyText: false });
  };

  render() {
    const helpTextClasses = ["help", styles.helpText, styles.hideText];
    if (this.state.showCopyText) {
      helpTextClasses.pop();
    }
    return (
      <div>
        <Button
          className={"clicker " + styles.urlText}
          id="text"
          data-clipboard-target="#text"
        >
          {this.props.text}
        </Button>
        <Button
          className={"clicker " + styles.copyButton}
          data-clipboard-target="#text"
        >
          <img className={styles.icon} src={Clippy} alt="Copy to clipboard" />
        </Button>
        {this.props.enableToolTip && (
          <p className={helpTextClasses.join(" ")}>Copied to clipboard! </p>
        )}
      </div>
    );
  }
}
