import { uniqueNamesGenerator, colors, animals } from "unique-names-generator";
import { capitalize } from "../utils/capitalize";
export function randomNameGenerator() {
  const config = {
    separator: " ",
    length: 2,
    dictionaries: [colors, animals]
  };
  const name = uniqueNamesGenerator(config);
  return name
    .split(" ")
    .map(capitalize)
    .join("");
}
