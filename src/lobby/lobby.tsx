import React from "react";
import Cookies from "react-cookies";
import { ServerConnection } from "./connection";

import {
  LobbyState,
  LobbyProps,
  GameInfo,
  SimpleProps,
  RequestResult
} from "./lobbyTypes";
import { ToggleScreen } from "./toggleScreen";
import { LoginScreen } from "./loginScreen";
import { randomNameGenerator } from "./randomName";
import { HomeScreen } from "./homeScreen";
import { PlayScreen } from "./playScreen";
import { Loading } from "../utils/loading";
import Mousetrap from "mousetrap";
import { fullScreen } from "../utils/fullScreen";
import globalStyles from "../globalStyles.module.css";

/**
 * Lobby
 *
 * React lobby component.
 *
 * @param {Array}  gameComponents - An array of Board and Game objects for the supported games.
 * @param {string} lobbyServer - Address of the lobby server (for example 'localhost:8000').
 *                               If not set, defaults to the server that served the page.
 * @param {string} gameServer - Address of the game server (for example 'localhost:8001').
 *                              If not set, defaults to the server that served the page.
 * @param {function} clientFactory - Function that is used to create the game clients.
 * @param {bool}   debug - Enable debug information (default: false).
 *
 * Returns:
 *   A React component that provides a UI to create, list, join, leave, play or spectate game instances.
 */

export default class Lobby extends React.Component<LobbyProps, LobbyState> {
  connection: any;
  suggestedName = randomNameGenerator();

  constructor(props: LobbyProps) {
    super(props);

    let cookie = Cookies.load("LobbyState") || {};
    console.log("Restoring session, cookies: ", cookie);
    this.state = {
      referrer: "/create",
      playerName: cookie.playerName || null,
      savedGames: cookie.savedGames || {},
      errorMsg: "",
      justAuthenticated: true,
      justCreatedGameId: null,
      createGameStatus: null,
      setNameStatus: null
    };
    this._setDocumentTitle();
    this.connection = new ServerConnection(this.props.lobbyServer);
  }

  componentDidMount() {
    Mousetrap.bind(["ctrl+shift+f", "command+shift+f"], fullScreen);

    this.resolveRedirects();
  }

  componentWillUnmount() {
    Mousetrap.unbind(["ctrl+shift+f", "command+shift+f"]);
  }

  componentDidUpdate(prevProps: LobbyProps) {
    // Save state
    let cookie = {
      referrer: this.state.referrer,
      playerName: this.state.playerName,
      savedGames: this.state.savedGames,
      errorMsg: ""
    };
    console.log("Saving cookie: ", cookie);
    Cookies.save("LobbyState", cookie, {
      path: "/"
    });
    if (this.props.currentScreen !== prevProps.currentScreen) {
      this._setDocumentTitle();
    }

    this.resolveRedirects();
  }

  _setDocumentTitle() {
    document.title =
      "Games - " +
      {
        home: "Create Game",
        play: `Playing ${this.props.match.params.gameId}`,
        login: "Login",
        join: ""
      }[this.props.currentScreen];
  }

  async resolveRedirects() {
    if (!this.isAuthenticated() && this.props.currentScreen !== "login") {
      console.log(
        "Redirecting to login page, referrer: ",
        this.props.match.url
      );
      this.setState({
        referrer:
          this.props.match.url === "/login" ? "/create" : this.props.match.url
      });
      this._redirect("/login");
    } else if (
      this.isAuthenticated() &&
      this.props.currentScreen === "login" &&
      this.state.justAuthenticated
    ) {
      this._redirect(this.state.referrer);
      this.setState({
        setNameStatus: { result: "success" }
      });
    } else if (this.state.justCreatedGameId) {
      this._redirect(`/play/${this.state.justCreatedGameId}`);
    }
  }

  async _leaveInstance(gameID: string) {
    if (this.state.savedGames.hasOwnProperty(gameID)) {
      await this.connection.leave(gameID, this.state.savedGames[gameID]);
      this.setState(prevState => {
        const newSavedGames = {
          ...prevState.savedGames
        };
        delete newSavedGames[gameID];
        return { savedGames: newSavedGames };
      });
    } else {
      throw new Error(`You are not in the game ${gameID}`);
    }
  }

  _leaveAllInstances() {
    for (const gameID in this.state.savedGames) {
      if (this.state.savedGames.hasOwnProperty(gameID)) {
        if (gameID !== undefined) {
          this._leaveInstance(gameID);
        }
      }
    }
  }

  _redirect(path: string) {
    this.props.history.push(path);
    this.setState({
      justAuthenticated: false,
      justCreatedGameId: null
    });
  }

  _createInstance = async (gameName: string) => {
    //Create instance and join the instance
    try {
      const gameInfo = await this.connection.create(
        this.state.playerName,
        gameName,
        this.props.gameComponents[gameName].game.maxPlayers
      );
      console.log("Created game ", gameInfo);
      this._saveGame(gameInfo);
      this.setState({
        justCreatedGameId: gameInfo.gameID
      });
    } catch (error) {
      this.setState({ errorMsg: error.message });
    }
  };

  _saveGame = (gameInfo: GameInfo) => {
    const gameEntry = {
      [gameInfo.gameID]: gameInfo
    };
    console.log("Saving game: ", gameEntry);
    this.setState((prevState: LobbyState, prevProps: LobbyProps) => {
      return {
        savedGames: {
          ...prevState.savedGames,
          ...gameEntry
        }
      };
    });
  };

  _joinInstance = async (gameID: string): Promise<RequestResult> => {
    console.log("Requesting credentials for ", gameID);
    console.log(this.state.savedGames);
    // Get user credentials and save them
    if (this.state.savedGames[gameID]) {
      throw new Error("We already have credentials for this game.");
    }
    try {
      const { playerID, credentials, gameName } = await this.connection.join(
        gameID,
        this.state.playerName
      );
      const gameInfo = {
        gameName,
        gameID,
        playerID,
        credentials
      };
      this._saveGame(gameInfo);
      return { result: "success" };
    } catch (error) {
      this.setState({ errorMsg: error.message });
      return {
        result: "fail",
        msg: error.message
      };
    }
  };

  isAuthenticated = () => {
    return this.state.playerName !== null;
  };

  onClickLeave = (gameID: string) => {
    this._redirect("/create");
    this._leaveInstance(gameID);
  };

  onClickLogin = (input: string) => {
    this.setState({
      playerName: input,
      justAuthenticated: true,
      setNameStatus: { result: "pending" }
    });
  };

  onClickLogout = () => {
    this._leaveAllInstances();
    this._redirect("/login");
    const defaultState = {
      referrer: "/create",
      playerName: null,
      savedGames: {},
      errorMsg: "",
      justAuthenticated: false,
      justCreatedGameId: null,
      createGameStatus: null,
      setNameStatus: null
    };
    console.log("logging out");
    this.setState(defaultState);
  };

  onClickCreate = async (gameName: string) => {
    this.setState({
      createGameStatus: { result: "pending" }
    });
    await this._createInstance(gameName);
    this.setState({
      createGameStatus: { result: "success" }
    });
  };

  render() {
    return (
      <React.Fragment>
        <button
          onClick={fullScreen}
          style={{
            position: "absolute",
            opacity: 0,
            width: "50px",
            height: "50px",
            zIndex: 3
          }}
        >
          Enable debug mode
        </button>
        <div className={globalStyles.bottomNavBar} />
        <ToggleScreen enabled={this.props.currentScreen === "login"}>
          <LoginScreen
            loginAction={this.onClickLogin}
            suggestedName={this.suggestedName}
            setNameStatus={this.state.setNameStatus}
          />
        </ToggleScreen>

        <ToggleScreen enabled={this.props.currentScreen === "play"}>
          <PlayScreen
            enabled={
              this.props.currentScreen === "play" && this.isAuthenticated()
            }
            joinAction={this._joinInstance}
            urlGameId={this.props.match.params.gameId as string}
            runningGames={this.state.savedGames}
            gameComponents={this.props.gameComponents}
            clientFactory={({ game, board }: SimpleProps) =>
              this.props.clientFactory({
                numPlayers: game.maxPlayers,
                game,
                board,
                debug: this.props.debug,
                multiplayer: {
                  server: this.props.gameServer
                },
                loading: () => Loading("Loading...")
              })
            }
            leaveAction={this.onClickLeave}
          />
        </ToggleScreen>

        <ToggleScreen enabled={this.props.currentScreen === "home"}>
          <HomeScreen
            logoutAction={this.onClickLogout}
            enterAction={(gameName: string) => this.onClickCreate(gameName)}
            gameComponents={this.props.gameComponents}
            defaultGame={this.props.defaultGame}
            createGameStatus={this.state.createGameStatus}
          />
        </ToggleScreen>
      </React.Fragment>
    );
  }
}
