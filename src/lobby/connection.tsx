/*
 * Copyright 2018 The boardgame.io Authors
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */
import { GameInfo } from "./lobbyTypes";
function assert(condition: boolean, message: string) {
  if (!condition) {
    message = message || "Assertion failed";
    if (typeof Error !== "undefined") {
      throw new Error(message);
    }
    throw message; // Fallback
  }
}

export class ServerConnection {
  server: string;
  constructor(server: string) {
    this.server = server;
  }

  _baseUrl() {
    return `${this.server || ""}/games`;
  }

  async join(gameID: string, playerName: string) {
    try {
      assert(
        typeof gameID === "string",
        "Game name should be a string, not " + gameID
      );
      // TODO: Leave all games!!

      const resp = await fetch(this._baseUrl() + "/" + gameID + "/join", {
        method: "POST",
        body: JSON.stringify({
          playerName: playerName
        }),
        headers: { "Content-Type": "application/json" }
      });
      if (resp.status !== 200) throw new Error("" + resp.status);
      const json = await resp.json();
      console.log(
        `Joined game ${json.gameName} as playerID: ${
          json.playerID
        }, with credentials: ${json.credentials}`
      );

      return {
        credentials: json.credentials,
        playerID: json.playerID,
        gameName: json.gameName
      };
    } catch (error) {
      console.log("" + error);
      const statusToMsg = (status: string): string => {
        const responseCode = status.slice(-3);
        if (responseCode === "409") {
          return `Sorry, the game ${gameID} is full!`;
        } else if (responseCode === "404") {
          return `The game ${gameID} doesn't exist. Check your URL.`;
        } else if (responseCode === "403" || responseCode === "401") {
          return "You need to choose a name before you join.";
        } else {
          return "Error joining game. Log out and try again.";
        }
      };

      throw new Error(statusToMsg(error + ""));
    }
  }

  async leave(gameID: string, gameInfo: GameInfo) {
    assert(
      typeof gameID === "string",
      "Game name should be a string of length 6, not " + gameID
    );

    assert(gameInfo.credentials !== undefined, "Need credentials to leave!");

    console.log("Leaving ", gameInfo);

    try {
      console.log("Leaving room, playerID: ", gameInfo.playerID);
      const resp = await fetch(this._baseUrl() + "/" + gameID + "/leave", {
        method: "POST",
        body: JSON.stringify({
          playerID: gameInfo.playerID,
          credentials: gameInfo.credentials
        }),
        headers: { "Content-Type": "application/json" }
      });
      if (resp.status !== 200) {
        throw new Error("HTTP status " + resp.status);
      }
    } catch (error) {
      throw new Error("failed to leave room " + gameID + " (" + error + ")");
    }
  }

  async create(playerName: string, gameName: string, numPlayers: number) {
    try {
      const resp = await fetch(this._baseUrl() + "/" + gameName + "/create", {
        method: "POST",
        body: JSON.stringify({
          playerName,
          numPlayers
        }),
        headers: { "Content-Type": "application/json" }
      });
      if (resp.status !== 200) throw new Error("HTTP status " + resp.status);
      const json = await resp.json();
      return json;
    } catch (error) {
      throw new Error(
        "failed to create room for " + gameName + " (" + error + ")"
      );
    }
  }
}
