import React from "react";
import { Select } from "antd";
import { HomeScreenProps, HomeScreenState } from "./lobbyTypes";
import globalStyles from "../globalStyles.module.css";
import { Button } from "antd";
import styles from "./homeScreen.module.css";
import { GameDescription } from "./gameDescription";
const { Option } = Select;
export class HomeScreen extends React.Component<
  HomeScreenProps,
  HomeScreenState
> {
  constructor(props: HomeScreenProps) {
    super(props);
    this.state = {
      selectedGame: this.props.defaultGame
    };
  }

  _onEnterAction = async () => {
    await this.props.enterAction(this.state.selectedGame);
  };

  _handleChange = (value: string) =>
    this.setState({
      selectedGame: value
    });

  render() {
    return (
      <>
        <div className={globalStyles.navBar}>
          <Button
            type="default"
            onClick={this.props.logoutAction}
            className={globalStyles.topRightButton}
          >
            Logout
          </Button>
        </div>

        <div className={globalStyles.screen}>
          <div className={globalStyles.uiScreen}>
            <div className={globalStyles.instructions}>
              Choose a game.</div>
              <Select
                defaultValue={this.props.defaultGame}
                style={{ width: 200 }}
                size="large"
                onChange={this._handleChange}
              >
                {Object.values(this.props.gameComponents).map(
                  (components, idx) => (
                    <Option value={components.game.name} key={idx}>
                      {components.game.displayName}
                    </Option>
                  )
                )}
              </Select>
           
              <GameDescription description={this.props.gameComponents[this.state.selectedGame].description}/>
            <Button
              loading={
                this.props.createGameStatus !== null &&
                this.props.createGameStatus.result === "pending"
              }
              size="large"
              type="primary"
              onClick={this._onEnterAction}
            >
              Create Game
            </Button>
          </div>
        </div>
      </>
    );
  }
}
