import * as React from 'react';

export interface IGameDescriptionProps {
  description: string
}

export function GameDescription (props: IGameDescriptionProps) {
  return (
    <div>
    {props.description}
    </div>
  );
}
