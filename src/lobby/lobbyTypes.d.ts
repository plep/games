export interface GameComponent {
  game: {
    name: string;
    maxPlayers: number;
    minPlayers: number;
    displayName: string;
  };
  board: any;
  description: string;
}

export interface SimpleProps {
  game: any;
  board: any;
}

interface ClientFactoryProps extends SimpleProps {
  numPlayers: number;
  debug: boolean;
  multiplayer: { server: string };
  loading: any;
}

export interface RoutePassProps {
  history: { push: any; [key: string]: any };
  location: any;
  match: {
    params: { gameId?: string };
    url: string;
  };
}

export interface CredentialStore {
  [key: string]: string;
}

export type DimensonSetter = (width: number, height: number) => void;

export interface GameInfo {
  gameID: string;
  gameName: string;
  playerID: string;
  credentials: string;
}

interface LobbyState {
  playerName: string | null;
  savedGames: { [key: string]: GameInfo };
  errorMsg: string;
  referrer: string;
  justAuthenticated: boolean;
  justCreatedGameId: string | null;
  createGameStatus: RequestStatus | null;
  setNameStatus: RequestStatus | null;
}

export type ScreenLabel = "home" | "play" | "login" | "join";

interface LobbyProps extends RoutePassProps {
  gameComponents: { [key: string]: GameComponent };
  lobbyServer: string;
  gameServer: string;
  debug: boolean;
  clientFactory: (props: ClientFactoryProps) => any;
  dimensionSetter?: DimensonSetter;
  defaultGame: string;
  currentScreen: ScreenLabel;
  history: any;
}

export interface JoinFormProps {
  joinRoomFunc: (gameID: string) => void;
  playerName: string | null;
  initialGameId: string;
}

export interface HomeScreenProps {
  enterAction: (gameName: string) => void;
  gameComponents: { [key: string]: GameComponent };
  defaultGame: string;
  createGameStatus: RequestStatus | null;
  logoutAction: () => void;
}

export interface HomeScreenState {
  selectedGame: string;
}
type RequestResult = { result: "success" } | { result: "fail"; msg: string };
type RequestStatus = RequestResult | { result: "pending" };

export interface PlayScreenProps {
  joinAction: (gameID: string) => Promise<RequestResult>;
  enabled: boolean;
  urlGameId: string;
  runningGames: { [key: string]: GameInfo };
  gameComponents: { [key: string]: GameComponent };
  clientFactory: (props: SimpleClientFactoryProps) => any;
  leaveAction: (gameID: string) => void;
}

export interface PlayScreenState {
  joinRequestStatuses: {
    [key: string]: RequestStatus;
  };
}

export interface ClientLobbyProps extends LobbyProps {
  initialGameId: string;
  directJoin: boolean;
}

export interface ClientLobbyState extends LobbyState {
  initialGameId: string;
  directJoin: boolean;
  hasChosenNameBefore: boolean;
}
export interface LobbyConnectionOpts {
  server: any;
}

export interface GameInstance {
  gameID: string;
  players: Player[];
}

export interface Player {
  id: number;
  name: string;
}
export interface JoinScreenProps {
  joinAction: (gameID: string) => void;
  match: {
    params: object;
    url: string;
  };
}

export interface JoinScreenState {
  gameID: string;
}
export interface LoginScreenProps {
  loginAction: (input: string) => void;
  suggestedName: string;
  setNameStatus: RequestStatus | null;
}

export interface LoginScreenState {
  input: string;
}
