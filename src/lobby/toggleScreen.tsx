import React from "react";
import styles from "./toggleScreen.module.css";

interface ToggleScreenProps {
  enabled: boolean;
}

export class ToggleScreen extends React.Component<ToggleScreenProps> {
  render() {
    return (
      <div className={this.props.enabled ? styles.visible : styles.hidden}>
        {this.props.children}
      </div>
    );
  }
}
