import React from "react";
import globalStyles from "../globalStyles.module.css";
import { PlayScreenProps, PlayScreenState, RequestStatus } from "./lobbyTypes";
import { Button, Result } from "antd";
import { Loading } from "../utils/loading";
export class PlayScreen extends React.Component<
  PlayScreenProps,
  PlayScreenState
> {
  constructor(props: PlayScreenProps) {
    super(props);
    this.state = {
      joinRequestStatuses: {}
    };
  }

  async fetchCredentials() {
    const { urlGameId, runningGames } = this.props;
    const joinRequestStatuses = this.state.joinRequestStatuses;
    if (
      !runningGames.hasOwnProperty(urlGameId) &&
      (!joinRequestStatuses.hasOwnProperty(urlGameId) ||
        joinRequestStatuses[urlGameId].result === "success")
      // Otherwise we could have a successful request from the past
      // blocking all future requests
    ) {
      this._saveJoinRequestStatus(urlGameId, { result: "pending" });
      console.log(`Attempting to join game ${urlGameId}`);
      const joinRequestResult = await this.props.joinAction(urlGameId);
      this._saveJoinRequestStatus(urlGameId, joinRequestResult);
    }
  }

  _saveJoinRequestStatus(gameID: string, status: RequestStatus) {
    const newEntry = { [gameID]: status };
    this.setState(prevState => {
      return {
        joinRequestStatuses: { ...prevState.joinRequestStatuses, ...newEntry }
      };
    });
  }

  componentDidMount() {
    if (this.props.enabled) {
      this.fetchCredentials();
    }
  }

  componentDidUpdate() {
    if (this.props.enabled) {
      this.fetchCredentials();
    }
  }

  render() {
    const { urlGameId, runningGames } = this.props;
    const activeJoinRequestStatus = this.state.joinRequestStatuses[urlGameId];
    let renderComponent;
    if (runningGames.hasOwnProperty(urlGameId)) {
      const { gameID, credentials, playerID, gameName } = runningGames[
        urlGameId
      ];
      const gameCode = this.props.gameComponents[gameName];
      const App = this.props.clientFactory({
        game: gameCode.game,
        board: gameCode.board
      });

      renderComponent = (
        <App gameID={gameID} playerID={playerID} credentials={credentials} />
      );
    } else if (
      activeJoinRequestStatus &&
      activeJoinRequestStatus.result === "fail"
    ) {
      renderComponent = (
        <Result status="error" subTitle={activeJoinRequestStatus.msg} />
      );
    } else if (
      activeJoinRequestStatus &&
      activeJoinRequestStatus.result === "success"
    ) {
      renderComponent = (
        <Result status="success" subTitle="Succesfully joined game" />
      );
    } else if (
      activeJoinRequestStatus &&
      activeJoinRequestStatus.result === "pending"
    ) {
      renderComponent = Loading(`Joining game ${urlGameId}...`);
    } else {
      renderComponent = <></>;
    }
    return (
      <>
        {" "}
        <div className={globalStyles.navBar}>
          <Button
            type="default"
            onClick={() => this.props.leaveAction(urlGameId)}
            className={globalStyles.topRightButton}
          >
            Leave
          </Button>
        </div>
        <div className={globalStyles.screen}>{renderComponent}</div>
      </>
    );
  }
}
