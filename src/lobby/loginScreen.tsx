/*
 * Copyright 2018 The boardgame.io Authors.
 *
 * Use of this source code is governed by a MIT-style
 * license that can be found in the LICENSE file or at
 * https://opensource.org/licenses/MIT.
 */

import React from "react";
import styles from "./loginScreen.module.css";
import globalStyles from "../globalStyles.module.css";
import { LoginScreenProps, LoginScreenState } from "./lobbyTypes";
import { Input } from "antd";
import { Button } from "antd";

export class LoginScreen extends React.Component<
  LoginScreenProps,
  LoginScreenState
> {
  state = {
    input: this.props.suggestedName
  };
  render() {
    if (false) {
    } else {
      return (
        <div className={globalStyles.screen}>
          <div className={globalStyles.uiScreen}>
            <div className={globalStyles.instructions}>What's your name?</div>

            <Input
              size="large"
              className={styles.inputText + " input"}
              type="text"
              value={this.state.input}
              onChange={e => this.setState({ input: e.target.value })}
              onKeyPress={this.onKeyPress}
            />
            <Button
              loading={
                this.props.setNameStatus !== null &&
                this.props.setNameStatus.result === "pending"
              }
              size="large"
              type="primary"
              className={styles.loginButton}
              onClick={this.loginAction}
              disabled={this.state.input.length === 0}
            >
              Enter
            </Button>
          </div>
        </div>
      );
    }
  }

  loginAction = () => {
    if (this.state.input.length > 0) {
      this.props.loginAction(this.state.input);
    }
  };

  onKeyPress = (event: any) => {
    if (event.key === "Enter") {
      this.loginAction();
    }
  };
}
