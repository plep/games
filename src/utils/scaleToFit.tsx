import React from "react";
import { Dimensions } from "../screenSizes";

interface ScaleToFitProps {
  direction: "width" | "height" | "auto";
  childDimensions: (orientation: "landscape" | "portrait") => Dimensions;
}

interface ScaleToFitState {
  width: number;
  height: number;
}

export class ScaleToFit extends React.PureComponent<
  ScaleToFitProps,
  ScaleToFitState
> {
  intervalIds: number[];
  constructor(props: ScaleToFitProps) {
    super(props);
    this.intervalIds = [];
    this.state = {
      width: window.innerWidth,
      height: window.innerHeight
    };
  }

  updateWindowDims = () => {
    const windowDimensions = {
      width: window.innerWidth,
      height: window.innerHeight
    };
    if (
      windowDimensions.height !== this.state.height ||
      windowDimensions.width !== this.state.width
    ) {
      this.setState(windowDimensions);
    }
  };

  componentDidMount() {
    // Firefox and apple standalone app has buggy resize API
    // When window resize event fires, one of the dimensions changes first
    // and then the other dimension changes after some delay
    // The delay is usually between 10 and 300ms.
    // So if we immediately update the window dimensions after resize event,
    // only one of the dimensions will be correct.
    // Thus we need to run the update multiple times to make sure we get the
    // correct dimensions
    window.addEventListener("resize", () => {
      this.updateWindowDims();
      const updateTimes = [50, 100, 150, 500, 1000, 5000];
      for (let i = 0; i < updateTimes.length; i++) {
        const timeoutId = window.setTimeout(
          this.updateWindowDims,
          updateTimes[i]
        );
        this.intervalIds.push(timeoutId);
      }
    });
  }

  printWindowDims() {
    const windowDimensions = {
      width: window.innerWidth,
      height: window.innerHeight
    };
    console.log(windowDimensions);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.updateWindowDims);
    for (let i = 0; i < this.intervalIds.length; i++) {
      window.clearTimeout(this.intervalIds[i]);
    }
  }

  transformInfo() {
    const windowDimensions = this.state;

    const viewRatio = windowDimensions.width / windowDimensions.height;

    const orientation =
      viewRatio > 1 ? ("landscape" as const) : ("portrait" as const);

    const childDimensions = this.props.childDimensions(orientation);

    const scaleFactors = {
      width: windowDimensions.width / childDimensions.width,
      height: windowDimensions.height / childDimensions.height
    };

    const contentRatio = childDimensions.width / childDimensions.height;

    const limitingDimension = viewRatio > contentRatio ? "height" : "width";

    const scaleFactor =
      this.props.direction === "auto"
        ? scaleFactors[limitingDimension]
        : scaleFactors[this.props.direction];
    return { scaleFactor, orientation };
  }

  render() {
    const { scaleFactor, orientation } = this.transformInfo();
    const transformStyle = {
      transform: `scale(${scaleFactor})`
    };

    // Center the child element horizontally and vertically
    const childStyle = {
      display: "flex" as const,
      flexDirection: "column" as const,
      justifyContent: "center" as const,
      alignItems: "center" as const,
      ...this.props.childDimensions(orientation)
    };
    return (
      <div style={transformStyle}>
        <div style={childStyle}>
          {React.Children.map(this.props.children, (child: any) =>
            React.cloneElement(child, { orientation })
          )}
        </div>
      </div>
    );
  }
}
