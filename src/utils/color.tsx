export class Color {
  rgba: { r: number; g: number; b: number; a: number };
  constructor(r: number, g: number, b: number, a: number) {
    this.rgba = { r, g, b, a };
  }

  str() {
    return `rgba(${this.rgba.r},${this.rgba.g}, ${this.rgba.b}, ${
      this.rgba.a
    })`;
  }

  fade(opacity: number) {
    const newColor = new Color(
      Math.floor(this.rgba.r * opacity + 255 * (1 - opacity)),
      Math.floor(this.rgba.g * opacity + 255 * (1 - opacity)),
      Math.floor(this.rgba.b * opacity + 255 * (1 - opacity)),
      this.rgba.a
    );
    return newColor;
  }
}

export function color(r: number, g: number, b: number, a: number) {
  return new Color(r, g, b, a);
}
