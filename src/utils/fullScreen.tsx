export function fullScreen() {
  const root = document.documentElement as any;
  if (root.requestFullscreen) {
    root.requestFullscreen();
  } else if (root.webkitRequestFullScreen) {
    root.webkitRequestFullScreen();
  }
}
