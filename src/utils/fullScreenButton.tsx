import React from "react";
import { Button, Icon } from "antd";
import { fullScreen } from "./fullScreen";

export class FullScreenButton extends React.Component {
  onPress_f = (event: any) => {
    if (event.keyCode === 70) {
      fullScreen();
    }
  };
  componentDidMount() {
    document.onfullscreenchange = () => this.forceUpdate();
  }

  render() {
    return (
      <Button
        onClick={fullScreen}
        style={{
          visibility: !document.fullscreen
            ? ("visible" as const)
            : ("hidden" as const)
        }}
      >
        <Icon type="fullscreen" />
      </Button>
    );
  }
}
