import React from "react";
import styles from "./lobby.module.css";


export function mockText(text: string, dimensions: {
  width: number;
  height: number;
}) {
  const component = (props: any) => (<div style={dimensions}>
    <h1>{text}</h1>
    {ObjData({ props })}
  </div>);
  return component;
};



const objStyle = {
  fontSize: "12px",
  width: "512px",
  textAlign: "left" as any
};

export function ObjData(obj: object) {
  return (<pre style={objStyle}>{JSON.stringify(obj, null, 2)}</pre>);
}
