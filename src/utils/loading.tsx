import { Spin } from "antd";
import globalStyles from "../globalStyles.module.css";
import React from "react";

export function Loading(tip: string = "") {
  return (
    <div className={globalStyles.screen}>
      <Spin tip={tip} size="large" />
    </div>
  );
}
