export function randomSample<T>(list: T[], n: number) {
  // Takes a random sample of length n, from a list, without replacement
  // Note: the output won't be shuffled
  var total = list.length;
  const output = [];
  var to_be_found = n;
  var prob = n / total;
  for (let i = 0; i < list.length; i++) {
    if (to_be_found !== 0) {
      if (Math.random() < prob) {
        to_be_found -= 1;
        output.push(list[i]);
      }
      total -= 1;
      prob = to_be_found / total;
    } else {
      break;
    }
  }
  return output;
}

export function shuffle<T>(list: T[]) {
  // Returns permutation of the given list
  const unClaimed = [...list];
  const output = [] as T[];
  while (unClaimed.length > 0) {
    const idx = Math.floor(Math.random() * unClaimed.length);
    output.push(unClaimed[idx]);
    unClaimed.splice(idx, 1);
  }
  return output;
}
