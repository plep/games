export interface Dimensions {
  width: number;
  height: number;
}

export const ScreenSizes = {
  portrait: { width: 411, height: 731 },
  landscape: { width: 731, height: 411 }
};
