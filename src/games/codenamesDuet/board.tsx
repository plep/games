import React from "react";
import { color } from "../../utils/color";
import styles from "./board.module.css";
import { ClientSetupScreen } from "../..//setupScreen/components";
import { HostSetupScreen } from "../../setupScreen/setupScreens";
import { ScaleToFit } from "../../utils/scaleToFit";
import { ScreenSizes } from "../../screenSizes";
import { Button, Drawer, message } from "antd";
import globalStyles from "../../globalStyles.module.css";
import { BoardProps } from "../../boardGame";
import { CardGrid } from "../codenamesComponents/cardGrid";
import codenamesStyles from "../codenamesComponents/codenames.module.css";
import { CoopScoreBoard } from "../codenamesComponents/scoreBoard";

type CodenamesDuetTeam = "green" | "white" | "black";
type Card = { id: number; word: string; team: CodenamesDuetTeam };

const COLORS = {
  green: color(37, 198, 91, 1),
  black: color(4, 4, 4, 1),
  white: color(200, 200, 200, 1),
  red: color(255, 44, 86, 1),
  blank: color(255, 228, 198, 1),
  textBlack: color(10, 10, 10, 1),
  textWhite: color(252, 252, 252, 1)
};

export class Board extends React.Component<
  BoardProps,
  { hintHelpVisible: boolean; guessHelpVisible: boolean }
> {
  state = { hintHelpVisible: false, guessHelpVisible: false };

  closeDrawer = () => {
    this.setState({ hintHelpVisible: false, guessHelpVisible: false });
  };

  openDrawer = () => {
    this.props.isActive
      ? this.setState({ guessHelpVisible: true })
      : this.setState({ hintHelpVisible: true });
  };

  player_role_to_id = (player_role: number): "0"|"1" => {
    return player_role.toString() as "0"| "1";
  };

  _playerType() {
    console.log("Determining player type, playerID: " + this.props.playerID);
    var type;
    if ([0, 1].map(this.player_role_to_id).includes(this.props.playerID as "0" | "1")) {
      console.log()
      type = "spymaster";
    } else {
      console.log(parseInt(this.props.playerID) + " is a spectator");
      type = "spectator";
    }
    console.log(`Your player ID is ${this.props.playerID} and you are are a ${type}`);
    return type;
  }

  render() {
    console.log(`Active on this turn: ${this.props.isActive}`)
    const playerType = this._playerType();
    const props = this.props;
    let screen;
    if (this.props.ctx.phase === "gameSetup") {
      if (playerType === "host") {
        return (
          <HostSetupScreen
            {...props}
            playerTitles={Array(2)
              .fill(0)
              .map((x, idx) => `Player ${idx + 1}`)}
          />
        );
      } else if (playerType !== "spectator") {
        return <ClientSetupScreen {...props} />;
      } else {
        return <div>Spectator Setup</div>;
      }
    } else {
      if (playerType === "spymaster") {
        screen = <SpyMasterBoard {...props} openDrawer={this.openDrawer} />;
      
      } else if (playerType === "spectator") {
        screen = <SpectatorBoard {...props} openDrawer={this.openDrawer} />;
      }
    }
    return (
      <>
        <ScaleToFit
          childDimensions={orientation => ScreenSizes[orientation]}
          direction={"auto"}
        >
          <div
            className={globalStyles.gameScreen + " " + globalStyles.noselect}
          >
            {screen}
          </div>
        </ScaleToFit>
        <HelpDrawerHinter
          onClose={this.closeDrawer}
          visible={this.state.hintHelpVisible}
        />
        <HelpDrawerGuesser
          onClose={this.closeDrawer}
          visible={this.state.guessHelpVisible}
        />
      </>
    );
  }
}
// Player info
// <span>PlayerID: {parseInt(this.props.playerID)}, player_role_to_id: {this.props.G.order}</span>

class SpyMasterBoard extends React.Component<
  BoardProps & { openDrawer: () => void }
> {

  player_role = parseInt(this.props.playerID)

  

  which_team = (card: Card): CodenamesDuetTeam => {
    // Returns the color that this player sees
    return this.props.G.players[parseInt(this.props.playerID)].card_teams[
      card.id
    ];
  };

  sortCards = (cards: Card[]) => {
    const cardsList = cards.slice();
    const teamNumber = (card: Card) => {
      return { green: 0, white: 1, black: 2 }[this.which_team(card)];
    };

    // Sort by team, then by whether revealed or not.
    // cardsList.sort((card1, card2) => {
    //   if (teamNumber(card1) < teamNumber(card2)) {
    //     return 1;
    //   } else if (teamNumber(card1) === teamNumber(card2)) {
    //     if (card1.team && !card2.team) {
    //       return 1;
    //     } else {
    //       return -1;
    //     }
    //   } else {
    //     return -1;
    //   }
    // });

    // Sort by team
    cardsList.sort((card1, card2) =>
      teamNumber(card1) > teamNumber(card2) ? 1 : -1
    );
    return cardsList;
  };

  onClick = (id: number) => {
    if (!this.props.G.cards[id].can_hit[this.player_role]) {
      message.warning("This card has already been fully revealed.");
    } else if (this.props.isActive) {
      this.props.moves.selectCard(id);
    } else if (this.props.ctx.gameover) {
      message.warning("The game is already over.");
    } else {
      message.warning(
        <>It's not your turn to select. Give a hint to the other player!</>
      );
    }
  };

  endSelect = () => {
    setTimeout(
      () => (document.getElementById("cardGrid") as HTMLElement).focus(),
      1000
    );
    this.props.moves.endSelect();
  };

  render() {
    const props = this.props;
    console.log('phase',props.ctx.phase)

    const passButton = (
      <Button
        key={1}
        type="danger"
        className={
          codenamesStyles.PassButton +
          " " +
          (props.isActive ? "" : codenamesStyles.hide)
        }
        onClick={this.endSelect}
        disabled={!props.isActive}
      >
        Pass
      </Button>
    );

    return (
      <>
        <CoopScoreBoard
          cards={props.G.cards}
          gameover={props.ctx.gameover}
          turnsRemaining={props.G.turns_remaining}
          winConditions={{ green: 15, white: 11 }}
          score={props.G.score}
          assassinated={props.G.assasinated}
        />
        <CardGrid
          showIndicator={true}
          cards={this.sortCards(props.G.cards)}
          cardComponent={CardBox}
          onClick={this.onClick}
          indicatorColor={this.which_team}
          player_role={this.player_role}
          passButton={passButton}
          helpButton={<HelpButton helpAction={props.openDrawer} />}
          roleName="spymaster"
        />

        <StatusText
          gameover={props.ctx.gameover}
          isActive={props.isActive}
          helpAction={props.openDrawer}
        />
      </>
    );
  }
}

function SpectatorBoard(props: BoardProps & { openDrawer: () => void }) {
  return (
    <>
      <CoopScoreBoard
        cards={props.G.cards}
        gameover={props.ctx.gameover}
        turnsRemaining={props.G.turns_remaining}
        winConditions={{ green: 15, white: 11 }}
        score={props.G.score}
        assassinated={props.G.assasinated}
      />
      <CardGrid
        cards={props.G.cards}
        cardComponent={CardBox}
        onClick={() => {}}
        player_role={0}
        showIndicator={false}
        roleName="spectator"
      />

      <StatusText
        gameover={props.ctx.gameover}
        isActive={props.isActive}
        helpAction={props.openDrawer}
      />
    </>
  );
}

type GameOver = { winner: any } | undefined;
interface StatusTextProps {
  isActive: boolean;
  gameover: GameOver;
  helpAction: () => void;
}

class StatusText extends React.Component<StatusTextProps> {
  render() {
    const props = this.props;
    let text;
    if (props.gameover) {
      if (props.gameover.winner === "players") {
        text = (
          <>
            Good work, <span className={styles.green}>team</span>!
          </>
        );
      } else {
        text = (
          <>
            You <span className={styles.red}> lose</span>.{" "}
          </>
        );
      }
    } else if (!props.isActive) {
      text = "Give a hint!";
    } else {
      text = "";
    }
    const classes = [codenamesStyles.codenamesStatusText, codenamesStyles.hide];
    if (text !== "") {
      classes.push(codenamesStyles.show);
    }
    return (
      <div key={1} className={classes.join(" ")}>
        {text}
      </div>
    );
  }
}

function HelpButton({ helpAction }: { helpAction: () => void }) {
  return <Button onClick={helpAction} shape="circle" icon="question" />;
}

interface CardBoxProps {
  card: any;
  indicatorColor: string | null;
  onClick: () => void;
  player_role: number;
}

export function CardBox(props: CardBoxProps) {
  const card = props.card;

  type Color = keyof typeof COLORS;
  const getTextColor = (revealed_type: string) => {
    if (revealed_type === "white" || revealed_type === "blank") {
      return COLORS.textBlack;
    } else {
      return COLORS.textWhite;
    }
  };

  // Fade a card if you can't select it anymore
  // const cardIsActive = card.can_hit[1] || card.can_hit[2];
  const can_hit =
     card.can_hit[props.player_role];

  const dont_fade =
    can_hit ||
    card.revealed_type === "black";

  const bgOpacity = dont_fade ? 1 : 0.15;

  const bgColor = COLORS[card.revealed_type as Color].fade(bgOpacity);

  // Hard to read white on faded color
  const bgStyle = {
    backgroundColor: bgColor.str(),
    color: dont_fade
      ? getTextColor(card.revealed_type).str()
      : COLORS.textBlack.fade(0.5).str()
  };

  const indicatorColor = props.indicatorColor
    ? COLORS[props.indicatorColor as Color]
    : COLORS["blank"];
  const otherPlayer = props.player_role === 0 ? 1 : 0;
  const otherPlayerCanHit = card.can_hit[otherPlayer];
  const indicatorStyle = {
    opacity: indicatorColor !== COLORS["blank"] ? 1 : 0, // Don't show if we are host
    backgroundColor: otherPlayerCanHit
      ? indicatorColor.str()
      : indicatorColor.fade(0.15).str()
  };

  return (
    <div className={styles.cardContainer}>
      <div className={styles.cardIndicator} style={indicatorStyle} />
      <button
        disabled={!can_hit}
        className={styles.CardBox + " " + codenamesStyles.CardBox}
        style={bgStyle}
        onClick={props.onClick}
      >
        {card.word}
      </button>
    </div>
  );
}

function HelpDrawerHinter({
  visible,
  onClose
}: {
  visible: boolean;
  onClose: () => void;
}) {
  return (
    <Drawer
      title="How to play"
      placement="right"
      closable={true}
      onClose={onClose}
      visible={visible}
      width="350"
    >
      <ol className={styles.instructionsList}>
        <li>This is a co-op game. Both players are on the same team.</li>
        <li>
          Your goal is to make the other player select the words marked with
          <FakeCardIndicator color="green" />.
          <ul>
            <li>
              Each turn, you are allowed to give <i>one English word</i> and
              <i> one number</i> as your hint.
            </li>
            <li>
              <b>Example:</b> If you see
              <FakeGrid type="hinter" />
              you could say <i>"Sound, 2"</i>.
            </li>
            <li>
              Be careful. If your teammate selects a word marked with
              <FakeCardIndicator color="black" />, you both lose!
            </li>
          </ul>
        </li>
        <li>
          The other player uses your hint to try to select the right word(s).
          <ul>
            <li>
              The other player is allowed to keep guessing until they select a
              <FakeCardIndicator color="black" />
              or <FakeCardIndicator color="white" />
              word.
            </li>
          </ul>
        </li>
        <li>
          Once the other player guesses the wrong word, or decides to pass, it
          is now their turn to give you a hint.
        </li>
        <li>
          Try to get all 15 <FakeCardIndicator color="green" /> cards before
          time runs out (11 turns).
        </li>
        <li>Have fun!</li>
      </ol>
    </Drawer>
  );
}

function HelpDrawerGuesser({
  visible,
  onClose
}: {
  visible: boolean;
  onClose: () => void;
}) {
  return (
    <Drawer
      title="How to play"
      placement="right"
      closable={true}
      onClose={onClose}
      visible={visible}
      width="350"
    >
      <ol className={styles.instructionsList}>
        <li>This is a co-op game. Both players are on the same team.</li>
        <li>
          The other player is trying to get you to select certain words on the
          board.
          <ul>
            <li>
              They are only allowed to say one word and one number as a hint.
            </li>
            <li>
              <b>Example:</b> If you see
              <FakeGrid type="guesser" />
              and the hint is <i>"Sound, 2"</i>, then you should probably choose{" "}
              <i>ear</i> and <i>music</i>.
            </li>
          </ul>
        </li>
        <li>
          Once you receive the hint, you are allowed to keep guessing until you
          get something wrong.
        </li>
        <li>If this happens, it is now your turn to give a hint.</li>
        <li>Try to get all 15 cards before time runs out (11 turns).</li>
        <li>Have fun!</li>
      </ol>
    </Drawer>
  );
}

function FakeCard(props: {
  word: string;
  indicatorColor: "green" | "black" | "white";
}) {
  return (
    <div className={styles.cardContainer}>
      <div
        className={styles.cardIndicator}
        style={{ backgroundColor: COLORS[props.indicatorColor].str() }}
      />
      <button
        className={styles.CardBox + " " + codenamesStyles.CardBox}
        style={{
          backgroundColor: COLORS.blank.str()
        }}
      >
        {props.word}
      </button>
    </div>
  );
}

function FakeCardIndicator(props: { color: "green" | "black" | "white" }) {
  return (
    <>
      {" "}
      <div
        style={{
          backgroundColor: COLORS[props.color].str()
        }}
        className={styles.fakeCardIndicator}
      />{" "}
    </>
  );
}

function FakeGrid({ type }: { type: "guesser" | "hinter" }) {
  const words = ["ear", "music", "eye", "sign"];
  const colors =
    type === "guesser"
      ? (["black", "green", "white", "green"] as const)
      : (["green", "green", "black", "white"] as const);
  return (
    <div className={styles.fakeGrid}>
      {words.map((word, idx) => (
        <FakeCard key={idx} word={word} indicatorColor={colors[idx]} />
      ))}
    </div>
  );
}
