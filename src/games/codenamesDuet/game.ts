import { ORIG, DUET, UNDERCOVER } from "../../word_list";
import { shuffle, randomSample } from "../../utils/random";
import { ICard, IGameState, Color } from "./gameTypes";

function generateGameSetup(
  dictionary: string[]
): { cards: ICard[]; secrets: Array<[Color, Color]> } {
  // Choose words
  var words = randomSample(dictionary, 25);
  words = shuffle(words);

  // Choose teams for cards
  const card_teams = shuffle(
    Array(1)
      .fill(0)
      .concat(Array(5).fill(1))
      .concat(Array(3).fill(2))
      .concat(Array(5).fill(3))
      .concat(Array(1).fill(4))
      .concat(Array(1).fill(5))
      .concat(Array(1).fill(6))
      .concat(Array(7).fill(7))
      .concat(Array(1).fill(8))
  ).map((v: 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8) => {
    return {
      0: ["black", "green"], // 1
      1: ["white", "green"], // 5
      2: ["green", "green"], // 3
      3: ["green", "white"], // 5
      4: ["green", "black"], // 1
      5: ["white", "black"], // 1
      6: ["black", "black"], // 1
      7: ["white", "white"], // 7
      8: ["black", "white"], // 1
    }[v];
  }) as Array<[Color, Color]>;

  // Make list of card objects
  const cards = [...Array(25)]
    .fill(0)
    .map((e, index) => Card(index, words[index]));

  return {
    cards: cards,
    // words: words,

    // Each player gets different secret information.
    secrets: card_teams,
    // The knowledge of each player that has been made public
  };
}

function Card(id: number, word: string): ICard {
  return {
    id: id,
    word: word,
    revealed_type: "blank", // The last revealed type of the card
    can_hit: { 0: true, 1: true }, // Which players can select this cards
    publicInfo: [null, null],
    who_revealed: undefined,
  };
}

function get_other_player(player: "0" | "1") {
  return player === "0" ? "1" : "0"; // player is always 0 or 1
}

export const CodeNamesDuet = {
  minPlayers: 2,
  maxPlayers: 2,
  name: "CodeNamesDuet",
  displayName: "Codenames: Duet",
  setup: () => {
    const G = {
      score: 0,
      assassinated: false,
      turns_remaining: 11,
    } as IGameState;
    const dictionary = ORIG.concat(DUET);
    Object.assign(G, generateGameSetup(dictionary));
    G.players = {
      0: { card_teams: G.secrets.map((pair) => pair[0]) },
      1: { card_teams: G.secrets.map((pair) => pair[1]) },
    };
    return G;
  },

  moves: {
    selectCard(G: IGameState, ctx: { [key: string]: any }, id: number) {
      const card = G.cards[id];
      const currentPlayer: "0" | "1" = ctx.currentPlayer;

      if (!card.can_hit[currentPlayer]) {
        console.log(
          "You've already chosen this card! Choose a different card."
        );
      } else {
        card.can_hit[currentPlayer] = false;
        card.who_revealed = currentPlayer;
        const otherPlayer = get_other_player(card.who_revealed);
        // Subtract 1 from index because G.secrets[id] has 2 elements,
        // while otherPlayer is either 1 or 2
        // The type that the other player sees
        const otherType = G.secrets[id][otherPlayer];
        card.publicInfo[otherPlayer] = otherType;
        card.revealed_type = otherType;

        if (card.revealed_type !== "white") {
          card.can_hit[otherPlayer] = false;
        }

        // If you didn't choose a good card, you have to end your turn.)
        if (card.revealed_type !== "green") {
          console.log("Wrong card!!!");
          G.turns_remaining = G.turns_remaining - 1;
          ctx.events.endTurn();
        } else {
          G.score += 1;
        }

        if (card.revealed_type === "black") {
          G.assassinated = true;
        }
      }
    },

    endSelect(G: IGameState, ctx: { [key: string]: any }) {
      // When you want to stop guessing.
      // ctx.events.endTurn(G.order[nextPlayer]);
      G.turns_remaining = G.turns_remaining - 1;
      ctx.events.endTurn();
    },
  },

  flow: {
    startingPhase: "guess",
    phases: {
      guess: {
        allowedMoves: ["selectCard", "endSelect"],
      },
    },
    endGameIf: (G: IGameState) => {
      if (G.assassinated || G.turns_remaining === 0) {
        return { winner: null };
      } else if (G.score >= 15) {
        return { winner: "players" };
      }
    },
  },
};
