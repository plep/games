export interface IGameState {
  cards: ICard[];
  secrets: Array<[Color, Color]>;
  turns_remaining: number;
  score: number;
  assassinated: boolean;
  players: { 0: { card_teams: Color[] }; 1: { card_teams: Color[] } };
}

export type Color = "green" | "white" | "black";

export interface ICard {
  id: number;
  word: string;
  revealed_type: "blank" | Color;
  can_hit: { 0: boolean; 1: boolean };
  publicInfo: [any, any];
  who_revealed?: "0" | "1";
}
