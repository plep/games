import { CodeNamesDuet as CodeNamesDuetGame } from "./game";
import { CodeNamesBoard } from "./board";

export const CodeNamesDuet = {
	game: CodeNamesDuetGame,
	board: CodeNamesBoard
};
