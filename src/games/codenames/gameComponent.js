import { CodeNames as CodeNamesGame } from "./game";
import { CodeNamesBoard } from "./board";

export const CodeNames = { game: CodeNamesGame, board: CodeNamesBoard, 
description: "A word game for 4+ players. Designed by Vladimír Chvátil." };
