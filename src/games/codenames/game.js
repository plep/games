import { Game } from "boardgame.io/core";
import { ORIG, DUET, UNDERCOVER } from "../../word_list";
import { shuffle, randomSample } from "../../utils/random";
// There are always four players:
// 0: Blue spymaster, gives hints to blue guessser
// 1: Blue guesser
// 2: Red spymaster, gives hints to red guessser
// 3: Red guesser

// 1000 most common english words

function generateGameSetup(dictionary) {
  // Choose words
  var words = randomSample(dictionary, 25);
  words = shuffle(words);

  // Choose teams for cards
  const card_teams = shuffle([
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    0,
    0,
    0,
    0,
    100,
    0,
    0,
    0,
    0,
    0,
    2,
    2,
    2,
    2,
    2,
    2,
    2
  ]).map(v => {
    var team = {
      1: "blue",
      0: "bystander",
      2: "red",
      100: "assassin"
    }[v];
    return team;
  });

  // Make list of card objects
  const cards = [...Array(25)].fill(0).map((e, index) => Card(index, words[index]));
  return {
    cards: cards,
    // words: words,

    // Only the spymasters know which teams the cards belong to
    // At the beginning of the game, we don't know which playerIDs
    // correspond to which roles
    secret: card_teams
  };
}

function Card(id, word) {
  return {
    id: id,
    word: word,
    who_revealed: null,
    team: null // this information is secret initially
  };
}

function which_team(card, G) {
  return G.secret[card.id];
}

function calculate_score(G) {
  // for performance, should write as update_score
  // assassinated is blue or red if they uncovered the assassin.
  const out = { blue: 0, red: 0, assassinated: null };
  console.log(G.cards);
  for (let i = 0; i < G.cards.length; i++) {
    const card = G.cards[i];
    if (card.who_revealed) {
      if (card.team === "blue") {
        out.blue += 1;
      } else if (card.team === "red") {
        out.red += 1;
      } else if (card.team === "assassin" && !out.assassinated) {
        out.assassinated = card.who_revealed;
      }
    }
  }
  return out;
}

function other_team(team) {
  if (team === "blue") {
    return "red";
  }

  if (team === "red") {
    return "blue";
  }
}

const customTO = {
  playOrder: (G, ctx) => ["0"].concat(G.order.map(i => i.toString())),
  first: (G, ctx) => {
    if (ctx.phase === "guess") {
      return 2;
    } else {
      return 0;
    }
  },
  next: (G, ctx) => (ctx.playOrderPos === 2 ? 4 : 2)
};

export const CodeNames = Game({
  minPlayers: 5,
  maxPlayers: 5,
  name: "CodeNames",
  displayName: "Codenames",
  setup: () => ({
    cards: [],
    active_team: "blue",
    win_conditions: { blue: 8, red: 7 },
    order: [1, 2, 3, 4]
  }),
  moves: {
    selectCard(G, ctx, id) {
      const card = G.cards[id];
      if (!card.team) {
        // Only do something if the card has not been revealed yet
        if (!card.who_revealed) {
          card.who_revealed = G.active_team;
          card.team = which_team(card, G);
        }

        // If you didn't choose a good card, you have to end your turn.
        if (G.active_team !== which_team(card, G)) {
          // const nextPlayer = G.active_team === 'blue' ? '2' : '0';
          // ctx.events.endTurn(G.order[nextPlayer]);
          ctx.events.endTurn(); // Goes to hint phase
          G.active_team = other_team(G.active_team);
        }
      }
    },

    endSelect(G, ctx) {
      // When you want to stop guessing.
      // ctx.events.endTurn(G.order[nextPlayer]);
      ctx.events.endTurn();
      // console.log('Next player is '+G.order[nextPlayer]) // Goes to hint phase
      G.active_team = other_team(G.active_team);
    },

    updateOrder(G, ctx, newArray) {
      // Whenever the host re-arranges players in the lobby
      G.order = newArray;
    },

    startGame(G, ctx) {
      console.log("Starting game...");
      // Generate the words
      const dictionary = ORIG.concat(DUET);
      Object.assign(G, generateGameSetup(dictionary));
      // Reveal secret info to the spymasters
      const order = G.order;
      G.players = {
        [order[0]]: G.secret,
        [order[1]]: {},
        [order[2]]: G.secret,
        [order[3]]: {}
      };
      ctx.events.endTurn();
      // console.log('G.order = ')
      // console.log(G.order)
      // ctx.events.endTurn(G.order[0]);
      ctx.events.endPhase();
    }
  },

  flow: {
    turnOrder: customTO,
    startingPhase: "gameSetup",
    phases: {
      gameSetup: {
        allowedMoves: ["startGame", "updateOrder"],
        next: "guess"
      },
      guess: {
        allowedMoves: ["selectCard", "endSelect"]
      }
    },

    endGameIf: (G, ctx) => {
      var winner = null;
      const score = calculate_score(G);
      console.log(score);

      for (let i = 0; i < 2; i++) {
        const team = ["blue", "red"][i];
        if (winner === null) {
          winner = score[team] >= G.win_conditions[team] ? team : null;
        }
      }

      if (score.assassinated) {
        winner = score.assassinated === "blue" ? "red" : "blue";
      }
      //If a team was assassinated, the other team wins
      // winner = score.assassinated ? other_team[score.assassinated] : null;
      if (winner != null) {
        console.log("Winner: " + winner);
        return { winner: winner };
      }
    }
  }
});
