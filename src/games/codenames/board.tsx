import React from "react";
import styles from "./board.module.css";
import { ClientSetupScreen } from "../../setupScreen/components";
import { HostSetupScreen } from "../../setupScreen/setupScreens";
import { ScaleToFit } from "../../utils/scaleToFit";
import { ScreenSizes } from "../../screenSizes";
import { Button, message } from "antd";
import codenamesStyles from "../codenamesComponents/codenames.module.css";
import globalStyles from "../../globalStyles.module.css";
import { BoardProps } from "../../boardGame";
import { CardGrid } from "../codenamesComponents/cardGrid";
import { ScoreBoard } from "../codenamesComponents/scoreBoard";
import { shuffle, randomSample } from "../../utils/random";

type CodenamesTeam = "blue" | "red" | "bystander" | "assassin";
type Card = { id: number; word: string; team: CodenamesTeam };

export class CodeNamesBoard extends React.Component<
  BoardProps,
  { transitionId: number | null }
> {
  constructor(props: BoardProps) {
    super(props);
    this.state = {
      transitionId: null
    };
  }

  beginTransition = (duration: number) => {
    if (this.state.transitionId) {
      window.clearTimeout(this.state.transitionId);
    }

    // window.setTimeout returns Id (unlike setTimeout)
    const transitionId = window.setTimeout(
      () =>
        this.setState({
          transitionId: null
        }),
      duration
    );
    this.setState({ transitionId });
  };

  componentDidUpdate(prevProps: BoardProps) {
    if (prevProps.G.active_team !== this.props.G.active_team) {
      this.beginTransition(3000);
    }
  }

  role_to_id = (role: number) => {
    return this.props.G.order[role].toString();
  };

  playerToTeam = (id: string) => {
    if ([0, 1].map(this.role_to_id).includes(id)) {
      return "blue";
    } else if ([2, 3].map(this.role_to_id).includes(id)) {
      return "red";
    } else {
      return "spectator";
    }
  };

  _playerType() {
    console.log("Determining player type, playerID: " + this.props.playerID);
    var type;
    if ([0, 2].map(this.role_to_id).includes(this.props.playerID)) {
      type = "spymaster";
    } else if ([1, 3].map(this.role_to_id).includes(this.props.playerID)) {
      type = "spyslave";
    } else if (this.props.playerID === "0") {
      type = "host";
    } else {
      console.log(this.props.playerID + " is a spectator");
      type = "spectator";
    }
    console.log("Player type: ", type);
    return type;
  }

  render() {
    const playerType = this._playerType();
    const props = {
      playerToTeam: this.playerToTeam,
      ...this.props
    };
    let screen;
    if (this.props.ctx.phase === "gameSetup") {
      if (playerType === "host") {
        return (
          <HostSetupScreen
            {...props}
            playerTitles={[
              "Blue Spymaster",
              "Blue Guesser",
              "Red Spymaster",
              "Red Guesser"
            ]}
          />
        );
      } else if (playerType !== "spectator") {
        return <ClientSetupScreen {...props} />;
      } else {
        return <div>Spectator Setup</div>;
      }
    } else {
      if (playerType === "spymaster") {
        screen = <SpyMasterBoard {...props} />;
      } else if (playerType === "spyslave") {
        screen = (
          <SpySlaveBoard
            {...props}
            playerToTeam={this.playerToTeam}
            transitionId={this.state.transitionId}
          />
        );
      } else if (playerType === "host") {
        // Spectators
        screen = (
          <SpectatorBoard {...props} transitionId={this.state.transitionId} />
        );
      } else if (playerType === "spectator") {
        // Spectators
        screen = (
          <SpectatorBoard {...props} transitionId={this.state.transitionId} />
        );
      }
    }
    return (
      <ScaleToFit
        childDimensions={orientation => ScreenSizes[orientation]}
        direction={"auto"}
      >
        <div className={globalStyles.gameScreen + " " + globalStyles.noselect}>
          {screen}
        </div>
      </ScaleToFit>
    );
  }
}

function SpyMasterBoard(props: BoardProps) {
  const which_team = (card: Card): CodenamesTeam => {
    return props.G.players[parseInt(props.playerID)][card.id];
  };

  const sortCards = (cards: Card[]) => {
    const cardsList = cards.slice();
    const teamNumber = (card: Card) => {
      return { blue: 0, red: 1, bystander: 2, assassin: 3 }[which_team(card)];
    };

    // Sort by team, then by whether revealed or not.
    // cardsList.sort((card1, card2) => {
    //   if (teamNumber(card1) < teamNumber(card2)) {
    //     return 1;
    //   } else if (teamNumber(card1) === teamNumber(card2)) {
    //     if (card1.team && !card2.team) {
    //       return 1;
    //     } else {
    //       return -1;
    //     }
    //   } else {
    //     return -1;
    //   }
    // });

    // Sort by team
    cardsList.sort((card1, card2) =>
      teamNumber(card1) > teamNumber(card2) ? 1 : -1
    );
    return cardsList;
  };

  return (
    <>
      <ScoreBoard
        cards={props.G.cards}
        active_team={props.G.active_team}
        winConditions={props.G.win_conditions}
        gameover={props.ctx.gameover}
      />
      <CardGrid
        showIndicator={true}
        cardComponent={CardBox}
        cards={sortCards(props.G.cards)}
        onClick={() => {}}
        indicatorColor={which_team}
        roleName="spymaster"
      />
      <StatusText gameover={props.ctx.gameover} />
    </>
  );
}

interface SpectatorBoardProps extends BoardProps {
  transitionId: number | null;
}

function SpectatorBoard(props: SpectatorBoardProps) {
  const onClick = (id: number) => {
    if (props.ctx.gameover) {
      message.warning("The game is already over.");
    } else if (props.transitionId !== null) {
      message.warning("It's not your turn.");
    } else if (props.G.cards[id].team) {
      message.warning("That card has already been selected.");
    } else if (props.isActive) {
      props.moves.selectCard(id);
    }
  };

  const colors = { red: "#ff4136", blue: "#0074d9" };
  const passButton = (
    <Button
      key={1}
      loading={props.transitionId !== null}
      type="default"
      className={
        codenamesStyles.PassButton +
        " " +
        (props.isActive ? "" : codenamesStyles.hide)
      }
      onClick={() => {
        props.moves.endSelect();
      }}
      style={{
        transition: "background-color 2s, opacity 1.5s, visibility 0s 1.5s",
        backgroundColor: colors[props.G.active_team as ("red" | "blue")],
        color: "white",
        borderColor: "rgb(217, 217, 217)"
      }}
    >
      Pass
    </Button>
  );
  return (
    <React.Fragment>
      <ScoreBoard
        cards={props.G.cards}
        active_team={props.G.active_team}
        winConditions={props.G.win_conditions}
        gameover={props.ctx.gameover}
      />
      <CardGrid
        cards={props.G.cards}
        onClick={onClick}
        cardComponent={CardBox}
        showIndicator={true}
        roleName="spectator"
        passButton={passButton}
      />
      <StatusText gameover={props.ctx.gameover} />
    </React.Fragment>
  );
}

interface SpySlaveBoardProps extends SpectatorBoardProps {
  playerToTeam: (playerID: string) => "blue" | "red" | "spectator";
}
function SpySlaveBoard(props: SpySlaveBoardProps) {
  const onClick = (id: number) => {
    if (props.ctx.gameover) {
      message.warning("The game is already over.");
    } else if (props.G.cards[id].team) {
      message.warning("That card has already been selected.");
    } else if (props.isActive) {
      props.moves.selectCard(id);
    } else {
      message.warning("It's not your turn.");
    }
  };

  const our_team = () => {
    return props.playerToTeam(props.playerID);
  };

  const colors = { red: "#ff4136", blue: "#0074d9" };
  const passButton = (
    <Button
      key={1}
      type="danger"
      className={
        codenamesStyles.PassButton +
        " " +
        (props.isActive ? codenamesStyles.show : codenamesStyles.hide)
      }
      style={{
        backgroundColor: colors[our_team() as ("blue" | "red")],
        color: "white",
        borderColor: "rgb(217, 217, 217)"
      }}
      onClick={() => props.moves.endSelect()}
      disabled={!props.isActive}
    >
      Pass
    </Button>
  );
  return (
    <CardGrid
      cardComponent={CardBox}
      showIndicator={false}
      cards={props.G.cards}
      onClick={onClick}
      passButton={passButton}
      roleName="spyslave"
    />
  );
}

function StatusText({ gameover }: { gameover: any }) {
  let team;
  if (gameover) {
    const playerTeam = gameover.winner;
    team = (
      <span className={styles.infoTeam + " " + styles[playerTeam]}>
        {playerTeam}
      </span>
    );
  } else {
    team = "";
  }
  const classes = [codenamesStyles.statusText, codenamesStyles.hide];
  if (team !== "") {
    classes.push(codenamesStyles.show);
  }
  return (
    <div key={1} className={classes.join(" ")}>
      Congratulations, {team}!
    </div>
  );
}

interface CardBoxProps {
  card: any;
  indicatorColor: string | null;
  onClick: () => void;
  roleName: "spectator" | "spymaster" | "spyslave";
  showIndicator: boolean;
}

function CardBox(props: CardBoxProps) {
  var classes = [styles.CardBox, codenamesStyles.CardBox];
  const card = props.card;
  if (card.team && props.showIndicator) {
    classes.push(styles[card.team]);
  } else if (props.indicatorColor !== null) {
    classes.push(styles[props.indicatorColor]);
  }
  const fade_revealed = props.roleName === "spymaster" && card.team;
  return (
    <button
      disabled={fade_revealed}
      className={classes.join(" ")}
      onClick={props.onClick}
    >
      {card.word}
    </button>
  );
}
