import { IGameState } from "../gameGenTypes";

export interface GameState extends IGameState {
  boardState: (SquareState[])[];
}

export interface SquareState {
  rowIdx: number;
  colIdx: number;
  owner: 0 | 1 | null;
}
