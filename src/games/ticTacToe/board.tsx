import React from "react";
import { IBoardProps } from "../gameGenTypes";
import styles from "./board.module.css";
import { GameState, SquareState } from "./gameTypes";

export class Board extends React.Component<IBoardProps<GameState>> {
  onClick = (rowIdx: number, colIdx: number) => {
    this.props.moves.selectSquare(rowIdx, colIdx);
  };

  render() {
    const props = this.props;
    return (
      <div className={styles.board}>
        {((props.G.boardState as any).flat() as SquareState[]).map(
          (squareState: SquareState, idx) => (
            <Square
              key={idx}
              squareState={squareState}
              onClick={() =>
                this.onClick(squareState.rowIdx, squareState.colIdx)
              }
            />
          )
        )}
      </div>
    );
  }
}

export class Square extends React.Component<{
  squareState: SquareState;
  onClick: () => void;
}> {
  idToSymbol(id: 0 | 1): string {
    return id === 0 ? "X" : "O";
  }

  render() {
    const props = this.props;
    const squareState = props.squareState;
    return (
      <div className={styles.square} onClick={props.onClick}>
        {squareState.owner !== null ? this.idToSymbol(squareState.owner) : ""}
      </div>
    );
  }
}
