import { SquareState, GameState } from "./gameTypes";
import { GameObject, GameCtx } from "../gameGenTypes";

function checkArraySame(arr: SquareState[]): 0 | 1 | null {
  // If all the entries in the input
  // are the same (and not null), return that entry

  if (arr.length === 0) {
    throw new Error("Array must have nonzero length");
  }
  let winner = null;
  for (let i = 0; i < arr.length; i++) {
    const val = arr[i].owner;
    if (val === null) {
      return null;
    }

    if (winner === null) {
      winner = val;
    }
    if (val !== winner) {
      return null;
    }
  }
  return winner;
}

function newSquareState(rowIdx: number, colIdx: number): SquareState {
  return {
    rowIdx,
    colIdx,
    owner: null
  };
}

export const Game = ((): GameObject<GameState, GameState> => {
  return {
    maxPlayers: 2,
    minPlayers: 2,
    name: "TicTacToe",
    displayName: "Tic-Tac-Toe",
    setup: () => {
      const boardState = Array(3)
        .fill(0)
        .map((x, rowIdx) =>
          Array(3)
            .fill(0)
            .map((y, colIdx) => newSquareState(rowIdx, colIdx))
        );
      return { boardState };
    },
    flow: {
      endGameIf: (G: GameState, ctx: GameCtx): { winner: number } | void => {
        const rows = G.boardState;
        const columns = [0, 1, 2].map(i =>
          [0, 1, 2].map(j => G.boardState[j][i])
        );
        const diagonal1 = [0, 1, 2].map(i => G.boardState[i][i]);
        const diagonal2 = [0, 1, 2].map(i => G.boardState[i][2 - i]);
        const diagonals = [diagonal1, diagonal2];

        const needToCheck = rows.concat(columns).concat(diagonals);
        for (let i = 0; i < needToCheck.length; i++) {
          const winner = checkArraySame(needToCheck[i]);
          if (winner !== null) {
            return { winner };
          }
        }
      }
    },
    moves: {
      selectSquare(G: GameState, ctx: GameCtx, rowIdx: number, colIdx: number) {
        const squareState = G.boardState[rowIdx][colIdx];
        if (squareState.owner === null) {
          squareState.owner = parseInt(ctx.currentPlayer) as 0 | 1;
        } else {
          throw new Error(
            `The square ${rowIdx},${colIdx} has already been filled.`
          );
        }
        ctx.events.endTurn();
      }
    }
  };
})();
