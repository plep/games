import { IGameCtx } from "./battleLine/gameTypes";

interface IBoardProps<GameState> {
  ctx: GameCtx;
  playerID: string;
  board: any;
  gameID: string;
  moves: any;
  G: Lan<GameState>;
}

type PlayerId = number;

type Move<GameState> = (G: GameState, ctx: GameCtx, ...args: any[]) => void;

export interface GameCtx {
  phase: string;
  playOrderPos: number;
  [key: string]: any;
}

export interface IGameState {
  players?: { [key: number]: any };
}

export interface GameObject<GameState extends IGameState, StripGameState> {
  minPlayers: number;
  maxPlayers: number;
  name: string; // The name that the server uses as identification
  displayName: string; // The name that is displayed to the client
  flow: {
    startingPhase?: string;
    phases?: { [key: string]: { allowedMoves: string[]; next?: string } };
    endGameIf: (
      G: GameState,
      ctx: GameCtx
    ) => { winner: PlayerId | string | null } | void;
  };
  // This runs when the player presses "Start Game".
  // It's a good place to initialize secret state
  // (Before "Start Game", players can sometimes change their roles)
  initGameState?: (G: GameState, ctx?: IGameCtx) => void;

  setup: () => GameState; // This runs as soon as the game is initialized
  playerView?: (G: GameState, ctx: GameCtx, playerId: string) => StripGameState;

  moves: { [key: string]: Move<GameState> };
}

export type Lan<GameState> = GameState & { order: PlayerId[] };
