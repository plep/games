import { ClientSetupScreen } from "../setupScreen/components";
import { HostSetupScreen } from "../setupScreen/setupScreens";
import React from "react";
import { IBoardProps } from "./gameGenTypes";

export function createLanBoard<GameState>(Board: any) {
  return class LanBoard<
    BoardProps extends IBoardProps<GameState>,
    BoardState
  > extends React.Component<BoardProps, BoardState> {
    render() {
      const props = this.props;
      let screen;
      if (props.ctx.phase === "gameSetup") {
        if (props.playerID == "0") {
          screen = (
            <HostSetupScreen
              {...props}
              playerTitles={Array(2)
                .fill(0)
                .map((x, idx) => `Player ${idx + 1}`)}
            />
          );
        } else {
          return <ClientSetupScreen {...props} />;
        }
      } else {
        screen = <Board {...props} />;
      }
      return screen;
    }
  };
}
