import { IStripGameState, IGameCtx } from "./game";
import { number } from "prop-types";

export type FlagInhabitants = [ICard[], ICard[]];

export interface ICard {
  suit: number;
  value: number;
  id: number;
}

export interface IBoardProps {
  G: IStripGameState;
  ctx: IGameCtx;
  moves: { [key: string]: any };
  isActive: boolean;
  playerID: string;
}

export interface IGameState {
  remainingCards: ICard[];
  hands: [ICard[], ICard[]];
  isCardPlayed: IsCardPlayed;
  flags: FlagInhabitants[];
  flagStatuses: FlagStatus[];
}

export interface ICard {
  suit: number;
  value: number;
  id: number;
}

export interface IStripGameState {
  remainingCards: ICard[];
  hand: ICard[];
  isCardPlayed: IsCardPlayed;
  flags: FlagInhabitants[];
  flagStatuses: FlagStatus[];
}

export interface IGameCtx {
  [key: string]: any;
}

export type IsCardPlayed = {
  [key in CardSuit]: { [key in CardValue]: boolean }
};

export type CardValue = number;
export type CardSuit = number;

export type FlagStatus = 0 | 1 | null;

export type FlagBound = { upper: HandStrength; lower: HandStrength };

export type HandStrength = {
  strength: "straightFlush" | "trips" | "flush" | "straight" | "host"; // 0 to 4
  values: number[]; // Sorted in descending order
};

export interface gameSettings {
  numberOfCardsInSuit: number;
  numberOfSuits: number;
  numberOfFlags: number;
  handSize: number;
}

// Use this interface for optimization

interface cardsBeenPlayed {
  availableInSuit: { [key in CardSuit]: number[] }; //  List of available values for given suit

  numberOfGivenSuit: { [key in CardSuit]: number };
  numberOfGivenValue: number[];
  isCardPlayed: IsCardPlayed;
}

// Update method:
// remove from available in suit
// decrement numberOfGivenSuit
// decrement numberOfGivenValue
// set isCardPlayed to true
