import { shuffle } from "../../utils/random";
import {
  IGameState,
  ICard,
  IGameCtx,
  FlagInhabitants,
  IsCardPlayed,
  CardValue,
  HandStrength,
  FlagBound,
  FlagStatus
} from "./gameTypes";

import { compareHandStrength, getHandBounds } from "./handStrengthLogic";

const PLAYERS = { 0: 0, 1: 1 };

const gameSettings = {
  numberOfCardsInSuit: 10,
  numberOfSuits: 6,
  numberOfFlags: 9,
  handSize: 7
};

function gameSetupFunc(G: IGameState) {
  G.remainingCards = shuffle(G.remainingCards);

  // Deal initial hands
  for (let i = 0; i < gameSettings.handSize; i++) {
    // Assume 2*handSize <= numberOfSuits*numberOfCardsInSuit
    G.hands[0].push(G.remainingCards.pop()!);
    G.hands[1].push(G.remainingCards.pop()!);
  }
}

export function cardConstructor(suit: number, value: number): ICard {
  return {
    suit,
    value,
    id: suit * gameSettings.numberOfCardsInSuit + value
  };
}

export function flagResolver(
  flagHands: [ICard[], ICard[]],
  isCardPlayed: IsCardPlayed
): 0 | 1 | null {
  const flagStrengths = [] as FlagBound[];
  for (let i = 0; i < 2; i++) {
    flagStrengths[i] = getHandBounds(flagHands[i], isCardPlayed);
  }

  const zeroWin = compareHandStrength(
    flagStrengths[0].lower,
    flagStrengths[1].upper
  );
  if (zeroWin === 1) {
    return 0;
  }
  const oneWin = compareHandStrength(
    flagStrengths[0].upper,
    flagStrengths[1].lower
  );
  if (oneWin === -1) {
    return 1;
  }
  return null;
}

const turnResolution = (
  flags: FlagInhabitants[],
  isCardPlayed: IsCardPlayed
) => {
  //Resolve all the flags
  // returns new flagStatuses
  const flagStatuses = [] as FlagStatus[];
  for (let i = 0; i < flags.length; i++) {
    flagStatuses[i] = flagResolver(flags[i], isCardPlayed);
  }
  return flagStatuses;
};

export const BattleLineObj = {
  minPlayers: 2,
  maxPlayers: 2,
  name: "BattleLine",
  displayName: "Battleline",
  setup: () => {
    const remainingCards = [];
    const isCardPlayed = {} as IsCardPlayed;
    for (let i = 0; i < gameSettings.numberOfSuits; i++) {
      isCardPlayed[i] = {} as { [key in CardValue]: boolean };
      for (let j = 0; j < gameSettings.numberOfCardsInSuit; j++) {
        const card = cardConstructor(i, j);
        remainingCards.push(card);
        isCardPlayed[i][j] = false;
      }
    }
    const flags = [] as FlagInhabitants[];
    const flagStatuses = [] as FlagStatus[];
    for (let i = 0; i < gameSettings.numberOfFlags; i++) {
      flags.push([[], []]);
      flagStatuses[i] = null;
    }
    const hands = [[], []] as [ICard[], ICard[]];
    const G = {
      remainingCards,
      isCardPlayed,
      hands,
      flags,
      flagStatuses
    };
    return G;
  },

  playerView: (G: IGameState, ctx: IGameCtx, playerID: string) => {
    return {
      flagStatuses: G.flagStatuses,
      remainingCards: G.remainingCards,
      isCardPlayed: G.isCardPlayed,
      flags: G.flags,
      hands: G.hands,
      hand: G.hands[parseInt(playerID)]
    };
  },

  flow: {
    endGameIf: (G: IGameState, ctx: IGameCtx) => {
      let winner = null;

      const DidPlayerWin = (id: 0 | 1): boolean => {
        let totalFlags = 0;
        let threeInARow = 0;
        // TODO: make gamesettings
        for (let i = 0; i < gameSettings.numberOfFlags; i++) {
          if (G.flagStatuses[i] === id) {
            totalFlags += 1;
            threeInARow += 1;
          } else {
            threeInARow = 0;
          }
          if (threeInARow === 3 || totalFlags >= 5) {
            return true;
          }
        }
        return false;
      };

      if (DidPlayerWin(0)) {
        return { winner: 0 };
      } else if (DidPlayerWin(1)) {
        return { winner: 1 };
      }
    }
  },

  initGameState(G: IGameState) {
    gameSetupFunc(G);
  },

  moves: {
    placeCard(G: IGameState, ctx: IGameCtx, handIdx: number, flagIdx: number) {
      const currentPlayerId = ctx.currentPlayer;
      const currentHand = G.hands[currentPlayerId];
      if (handIdx >= currentHand.length || handIdx < 0) {
        throw new Error(
          `Invalid card Id ${handIdx}: hand.length=${currentHand.length}`
        );
      }
      if (flagIdx >= gameSettings.numberOfFlags || flagIdx < 0) {
        throw new Error(
          `Invalid card Id ${handIdx}: hand.length=${currentHand.length}`
        );
      }

      if (G.flagStatuses[flagIdx] !== null) {
        throw new Error(
          `Player ${currentPlayerId} cannot play any more cards to flag ${flagIdx}.`
        );
      }
      const card = currentHand[handIdx];
      G.flags[flagIdx][currentPlayerId].push(card);
      G.isCardPlayed[card.suit][card.value] = true;
      if (G.remainingCards.length > 0) {
        currentHand[handIdx] = G.remainingCards.pop()!;
      } else {
        currentHand.splice(handIdx, 1);
      }

      G.flagStatuses = turnResolution(G.flags, G.isCardPlayed);
      ctx.events.endTurn();
    }
  }
};
