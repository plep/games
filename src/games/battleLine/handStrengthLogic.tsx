import {
  IGameState,
  ICard,
  IGameCtx,
  FlagInhabitants,
  IsCardPlayed,
  CardValue,
  HandStrength,
  FlagBound,
  FlagStatus
} from "./gameTypes";

// Flag Types:
// 4 - Straight Flush: Three cards of the same color with consecutive values. [R4][R5][R3]
// 3 - Trips: [Y8][R8][G8]
// 2 - Flush: Three cards of the same color. [B2][B7][B4]
// 1 - Straight: Three cards with consecutive values. [Y4][R6][G5]
// 0 - Host: Any other formation. [Y5][B5][G3]

const gameSettings = {
  numberOfCardsInSuit: 10,
  numberOfSuits: 6,
  numberOfFlags: 9,
  handSize: 7
};

function compareHandStrength(strength1: HandStrength, strength2: HandStrength) {
  const typesToNumber = {
    straightFlush: 4,
    trips: 3,
    flush: 2,
    straight: 1,
    host: 0
  };
  const s1 = typesToNumber[strength1.strength];
  const s2 = typesToNumber[strength2.strength];
  if (s1 > s2) {
    return 1;
  } else if (s1 < s2) {
    return -1;
  } else {
    for (let i = 0; i < 3; i++) {
      if (strength1.values[i] > strength2.values[i]) {
        return 1;
      } else if (strength1.values[i] < strength2.values[i]) {
        return -1;
      }
    }
  }
  return 0;
}

function computePossibleStraights(values: number[]) {
  // Given a list of 2 values, outputs the values which would make
  // it a straight of 3 values.
  const possibleStraights = [] as number[];

  if (
    values[0] === values[1] + 1 &&
    values[0] + 1 < gameSettings.numberOfCardsInSuit
  ) {
    // Top draw
    possibleStraights.push(values[1] + 2);
  }
  if (values[1] + 2 === values[0]) {
    // Middle draw
    possibleStraights.push(values[1] + 1);
  }
  if (values[1] + 1 === values[0] && values[1] - 1 >= 0) {
    // Bottom draw
    possibleStraights.push(values[1] - 1);
  }
  return possibleStraights;
}

function sortedHand(hand: ICard[]) {
  //sorts by value and then by suit
  const h = hand
    .slice()
    .sort((card1, card2) =>
      card1.value < card2.value
        ? 1
        : card1.value > card2.value
        ? -1
        : card1.suit > card2.suit
        ? 1
        : card1.suit < card2.suit
        ? -1
        : 0
    );
  return h;
}

function sortedStrength(handStrength: HandStrength): HandStrength {
  const sortedValues = handStrength.values;
  //Descending order
  sortedValues.sort((a, b) => b - a);
  return { strength: handStrength.strength, values: sortedValues };
}

function getHandBounds(hand: ICard[], isCardPlayed: IsCardPlayed) {
  const h = sortedHand(hand);
  let upper;
  let lower;
  if (h.length === 3) {
    upper = determineHandType(h);
    lower = upper;
  } else if (h.length === 2) {
    upper = getBestHand_2(hand, isCardPlayed);
    lower = getWorstHand_2(hand, isCardPlayed);
  } else if (h.length === 1) {
    upper = getBestHand_1(hand, isCardPlayed);
    lower = getWorstHand_1(hand, isCardPlayed);
  } else if (h.length === 0) {
    upper = getBestHand_0(isCardPlayed);
    lower = getWorstHand_0(isCardPlayed);
  }

  return {
    upper: sortedStrength(upper as HandStrength),
    lower: sortedStrength(lower as HandStrength)
  };
}

function determineHandType(hand: ICard[]): HandStrength {
  // Put in descending order
  const h = sortedHand(hand);
  let strength;
  if (h[0].value === h[1].value && h[1].value === h[2].value) {
    strength = "trips" as const;
  } else {
    const isStraight =
      h[0].value === h[1].value + 1 && h[2].value + 1 === h[1].value;
    const isFlush = h[0].suit === h[1].suit && h[1].suit === h[2].suit;
    if (isStraight && isFlush) {
      strength = "straightFlush" as const;
    } else if (isStraight) {
      strength = "straight" as const;
    } else if (isFlush) {
      strength = "flush" as const;
    } else {
      strength = "host" as const;
    }
  }
  return {
    strength: strength as
      | "trips"
      | "straightFlush"
      | "flush"
      | "straight"
      | "host",
    values: h.map((card: ICard) => card.value)
  };
}

// Fundamentally, we just need to be able to decide if side1.upperbound < side2.lowerbound or viceversa.
// Once we have side1.upperbound, to do the comparison, we just need to see if side2.lowerbound is greater than side1.upperbound.
// If we have sid2.lowerbound, we just need to see if side2.upperbound is lower than side2.lwoerbound.

// BestHandBounds: should also take into account what's on the other side.
// Then, for the upper bound, it suffices to check things that are larger than the lowerbound of the other side.
// For the lower bound, it suffices to check things that are lower than the upper

//For every incomplete hand, we want the Upper-> lower traversal

//And also the lower->upper traversal.
function getBestGoodHand_2(
  h: ICard[],
  isCardPlayed: IsCardPlayed
): HandStrength | null {
  // Give a partial hand with 2/3 cards,
  // Finds the best hand that can be made.
  // If we can't get anything better than a
  // host, return null.
  if (h[0].value === h[1].value) {
    for (let i = 0; i < gameSettings.numberOfSuits; i++) {
      if (!isCardPlayed[i][h[0].value]) {
        return {
          strength: "trips",
          values: [h[0].value, h[0].value, h[0].value]
        };
      }
    }
  } else {
    console.log(h[0].value, h[1].value);
    // These are not possible if trips is possible

    const flushPossible = h[0].suit === h[1].suit;
    const possibleStraights = computePossibleStraights([
      h[0].value,
      h[1].value
    ]);
    if (flushPossible) {
      const suit = h[0].suit;
      // First check for straight flushes
      for (let i = 0; i < possibleStraights.length; i++) {
        const value = possibleStraights[i];
        if (!isCardPlayed[suit][value]) {
          return {
            strength: "straightFlush",
            values: [h[0].value, h[1].value, value]
          };
        }
      }
      // Now check for normal flushes
      for (
        let value = gameSettings.numberOfCardsInSuit - 1;
        value >= 0;
        value--
      ) {
        if (!isCardPlayed[suit][value]) {
          return {
            strength: "flush",
            values: [h[0].value, h[1].value, value]
          };
        }
      }
    }

    // Check for normal straights
    for (let suit = 0; suit < gameSettings.numberOfSuits; suit++) {
      if (suit !== h[0].suit) {
        for (let i = 0; i < possibleStraights.length; i++) {
          const value = possibleStraights[i];
          if (!isCardPlayed[suit][value]) {
            return {
              strength: "straight",
              values: [h[0].value, h[1].value, value]
            };
          }
        }
      }
    }
  }
  return null;
}

function getBestHand_2(
  hand: ICard[],
  isCardPlayed: IsCardPlayed
): HandStrength {
  const h = sortedHand(hand);

  // Try to find something better than host
  const goodHand = getBestGoodHand_2(h, isCardPlayed);
  if (goodHand !== null) {
    return goodHand;
  }

  // Check for host
  for (let value = gameSettings.numberOfCardsInSuit - 1; value >= 0; value--) {
    for (let suit = 0; suit < gameSettings.numberOfSuits; suit++) {
      if (!isCardPlayed[suit][value]) {
        return {
          strength: "host",
          values: [h[0].value, h[1].value, value]
        };
      }
    }
  }
  console.log("Warning: Every single card has been checked.");
  return {
    strength: "host",
    values: [h[0].value, h[1].value, 0]
  };
}

function getWorstHand_2(
  hand: ICard[],
  isCardPlayed: IsCardPlayed
): HandStrength {
  // Only for length==2
  // Run time is O(number of cards played)
  const h = hand
    .slice()
    .sort((card1, card2) =>
      card1.value > card2.value ? 1 : card1.value < card2.value ? -1 : 0
    );
  if (h[0].value === h[1].value) {
    // If potential trips, we can always get host.
    for (let value = 0; value < gameSettings.numberOfCardsInSuit; value++) {
      for (let suit = 0; suit < gameSettings.numberOfSuits; suit++) {
        if (!isCardPlayed[suit][value] && value !== h[0].value) {
          return {
            strength: "host",
            values: [h[0].value, h[0].value, value]
          };
        }
      }
    }
  } else {
    // These are not possible if trips is possible

    const possibleFlushes = h[0].suit === h[1].suit ? [h[0].suit] : [];
    const possibleStraights = computePossibleStraights([
      h[0].value,
      h[1].value
    ]);
    // Find the worst
    let worst = { straightFlush: null, flush: null, straight: null } as {
      straightFlush: null | HandStrength;
      flush: null | HandStrength;
      straight: null | HandStrength;
    };
    for (let value = 0; value < gameSettings.numberOfCardsInSuit; value++) {
      for (let suit = 0; suit < gameSettings.numberOfSuits; suit++) {
        if (!isCardPlayed[suit][value]) {
          if (
            !possibleStraights.includes(value) &&
            !possibleFlushes.includes(suit)
          ) {
            // Not a flush and not a straight
            return {
              strength: "host",
              values: [h[0].value, h[1].value, value]
            };
          } // We're going from worst to best, so the first
          // encounter is guaranteed to be the worst.
          else if (
            possibleStraights.includes(value) &&
            !possibleFlushes.includes(suit)
          ) {
            // It's a normal straight
            if (worst.straight === null) {
              worst.straight = {
                strength: "straight",
                values: [h[0].value, h[1].value, value]
              };
            }
          } else if (
            possibleStraights.includes(value) &&
            !possibleFlushes.includes(suit)
          ) {
            // It's a normal flush
            if (worst.flush === null) {
              worst.flush = {
                strength: "flush",
                values: [h[0].value, h[1].value, value]
              };
            }
          } else {
            // It's a straightFlush
            if (worst.straightFlush === null) {
              worst.straightFlush = {
                strength: "straightFlush",
                values: [h[0].value, h[1].value, value]
              };
            }
          }
        }
      }
    }
    // We went through it all and couldn't do host
    const handType = [
      "straight" as const,
      "flush" as const,
      "straightFlush" as const
    ];

    for (let i = 0; i < 3; i++) {
      if (worst[handType[i]] !== null) {
        return worst[handType[i]]!;
      }
    }
  }
  console.log("Warning: Every single card has been checked.");
  return {
    strength: "host",
    values: [h[0].value, h[1].value, 0]
  };
}
/**
 * Determines the best hand that can be made given the cards that are already on the board
 * This particular function is for completeting hands with 1 card.
 * @param hand The cards that have been placed.
 * @param isCardPlayed A dictionary containing information on which cards have been played
 */
function getBestHand_1(
  hand: ICard[],
  isCardPlayed: IsCardPlayed
): HandStrength {
  const h = hand.slice();
  const thisSuit = h[0].suit;
  // This is the highest straight we can have
  const maxTop = Math.min(h[0].value + 2, gameSettings.numberOfCardsInSuit - 1);

  // Check for straightFlush
  for (let top = maxTop; top >= Math.max(2, maxTop - 2); top--) {
    // Check if top, top-1 and top-2 are available.
    const valuesOfStraight = [top, top - 1, top - 2];
    let straightFlushPossible = true;
    for (const value of valuesOfStraight) {
      if (value !== h[0].value && isCardPlayed[thisSuit][value]) {
        straightFlushPossible = false;
      }
    }
    if (straightFlushPossible) {
      return {
        strength: "straightFlush",
        values: valuesOfStraight
      };
    }
  }

  // Try trips
  let numberOfSameValue = 0;
  for (let i = 0; i < gameSettings.numberOfSuits; i++) {
    if (!isCardPlayed[i][h[0].value]) {
      numberOfSameValue += 1;
    }
    if (numberOfSameValue >= 2) {
      return {
        strength: "trips",
        values: [h[0].value, h[0].value, h[0].value]
      };
    }
  }

  // Try good flush
  const possibleFlushValues = [];
  for (let value = gameSettings.numberOfCardsInSuit - 1; value >= 0; value--) {
    if (!isCardPlayed[thisSuit][value]) {
      possibleFlushValues.push(value);
    }
    if (possibleFlushValues.length === 2) {
      return {
        strength: "flush",
        values: [h[0].value, possibleFlushValues[0], possibleFlushValues[1]]
      };
    }
    if (possibleFlushValues.length > 2) {
      throw new Error(
        `Length of possibleFlushValues.length is greater than 2 (${possibleFlushValues})`
      );
    }
  }

  // Try straight

  for (let top = maxTop; top >= Math.max(2, maxTop - 2); top--) {
    // Check if top, top-1 and top-2 are available.
    let straightPossible = true;
    const valuesOfStraight = [top, top - 1, top - 2];
    for (const value of valuesOfStraight) {
      if (value !== h[0].value && !isValuePlayable(value, isCardPlayed)) {
        // Don't need to worry about updating isValuePlayble becuase
        // When you're looking for a straight, you are
        // using unique values.
        straightPossible = false;
      }
    }
    if (straightPossible) {
      return {
        strength: "straight",
        values: valuesOfStraight
      };
    }
  }

  // Try Host BUG: NEED TO UPDATE ISVALUEPLAYABLE
  // BUG: some of these aren't actually hosts (actually are trips, but this shouldn't matter?)
  const possibleHost = [];
  for (let value = gameSettings.numberOfCardsInSuit - 1; value >= 0; value--) {
    if (isValuePlayable(value, isCardPlayed)) {
      possibleHost.push(value);
    }
    if (possibleHost.length === 2) {
      return {
        strength: "flush",
        values: [h[0].value, possibleHost[0], possibleHost[1]]
      };
    }
    if (possibleHost.length > 2) {
      throw new Error(
        `Length of possibleHost.length is greater than 2 (${possibleHost})`
      );
    }
  }
  // In this algorithm, we stop as soon as we get below the
  // For each node, we compute the _best_ possible.
  // We rank them from best to last.
  // Starting from best, we keep going down until we hit the best of the next node.
  // Then we do that.

  console.log("Warning: Every single card has been checked.");
  return {
    strength: "host",
    values: [h[0].value, h[1].value, 0]
  };
}

function getWorstHand_1(
  hand: ICard[],
  isCardPlayed: IsCardPlayed
): HandStrength {
  // Needs more work. No harm in underestimating here.
  return {
    strength: "host",
    values: [hand[0].value, 0, 0]
  };
}

function getWorstHand_0(isCardPlayed: IsCardPlayed): HandStrength {
  // Needs more work. No harm in underestimating here.
  return {
    strength: "host",
    values: [0, 0, 0]
  };
}

function getBestHand_0(isCardPlayed: IsCardPlayed): HandStrength {
  // Check for straightFlush

  for (let top = gameSettings.numberOfCardsInSuit - 1; top >= 2; top--) {
    for (let suit = 0; suit < gameSettings.numberOfSuits; suit++) {
      // Check if top, top-1 and top-2 are available.
      let straightPossible = true;
      for (let i = 0; i < 3; i++) {
        const value = [top, top - 1, top - 2][i];
        if (isCardPlayed[suit][value]) {
          straightPossible = false;
        }
      }
      if (straightPossible) {
        return {
          strength: "straightFlush",
          values: [top, top - 1, top - 2]
        };
      }
    }
  }

  // Check for trips

  for (let value = gameSettings.numberOfCardsInSuit - 1; value >= 0; value--) {
    let tripCounter = 0;
    for (let suit = 0; suit < gameSettings.numberOfSuits; suit++) {
      if (!isCardPlayed[suit][value]) {
        tripCounter += 1;
      }
      if (tripCounter === 3) {
        return {
          strength: "trips",
          values: [value, value, value]
        };
      }
    }
  }

  // Check for flush
  let bestFlush = null;
  for (let suit = 0; suit < gameSettings.numberOfSuits; suit++) {
    const flushPossibleValues = [];
    for (
      let value = gameSettings.numberOfCardsInSuit - 1;
      value >= 0;
      value--
    ) {
      if (!isCardPlayed[suit][value]) {
        flushPossibleValues.push(value);
      }
      if (flushPossibleValues.length === 3) {
        const flush = {
          strength: "flush" as const,
          values: flushPossibleValues
        };
        if (bestFlush === null || compareHandStrength(flush, bestFlush) === 1) {
          bestFlush = flush;
        }
        break;
      }
    }
  }
  if (bestFlush !== null) {
    return bestFlush;
  }

  // Check for straight
  for (let top = gameSettings.numberOfCardsInSuit - 1; top >= 2; top--) {
    // Check if top, top-1 and top-2 are available.
    let straightPossible = true;
    for (let i = 0; i < 3; i++) {
      const value = [top, top - 1, top - 2][i];
      if (!isValuePlayable(value, isCardPlayed)) {
        straightPossible = false;
      }
    }
    if (straightPossible) {
      return {
        strength: "straight",
        values: [top, top - 1, top - 2]
      };
    }
  }

  // Try Host BUG: NEED TO UPDATE ISVALUEPLAYABLE
  const possibleHost = [];
  for (let value = gameSettings.numberOfCardsInSuit - 1; value >= 0; value--) {
    if (isValuePlayable(value, isCardPlayed)) {
      possibleHost.push(value);
    }
    if (possibleHost.length === 3) {
      return {
        strength: "host",
        values: possibleHost
      };
    }
    if (possibleHost.length > 3) {
      throw new Error(
        `Length of possibleHost.length is greater than 3 (${possibleHost})`
      );
    }
  }
  console.log("WARNING: We tried everything");
  return {
    strength: "host",
    values: [0, 0, 0]
  };
}

// It is always possible to make a host or straight. Proof:
// There can be at most 6*9-1 = 53 cards on the board.
// Meaning that there are always at least 7 cards available to play.
// If first two are trips, then there are only 4/7 cards which give a non-host
// If first two is flush, then there are 8 possible ways to complete the flush.

// type CardSuit = "green" | "blue" | "red" | "purple" | "yellow" | "orange";

function isValuePlayable(
  value: CardValue,
  isCardPlayed: IsCardPlayed
): boolean {
  for (let suit = 0; suit < gameSettings.numberOfSuits; suit++) {
    if (!isCardPlayed[suit][value]) {
      return true;
    }
  }
  return false;
}

export { determineHandType, getBestHand_2, getHandBounds, compareHandStrength };
