import * as H from "./handStrengthLogic";
import {
  ICard,
  FlagInhabitants,
  FlagStatus,
  IsCardPlayed,
  CardValue,
  CardSuit
} from "./gameTypes";

const gameSettings = {
  numberOfCardsInSuit: 10,
  numberOfSuits: 6,
  numberOfFlags: 9,
  handSize: 7
};

const cardConstructor = (suit: CardSuit, value: CardValue): ICard => {
  return {
    suit,
    value,
    id: suit * gameSettings.numberOfCardsInSuit + value
  };
};

const listOfTuplesToCard = (lst: number[][]) => {
  // The type of the lst elements
  // should actually be [CardValue, CardNumber],
  // but it is inconvenient to define tuple literals in typescript,
  // so instead we use number[].
  return lst.map(([value, suit]) => cardConstructor(suit, value));
};

const setup = () => {
  const remainingCards = [];
  const isCardPlayed = {} as IsCardPlayed;
  for (let i = 0; i < gameSettings.numberOfSuits; i++) {
    isCardPlayed[i] = {} as { [key in CardValue]: boolean };
    for (let j = 0; j < gameSettings.numberOfCardsInSuit; j++) {
      const card = cardConstructor(i, j);
      remainingCards.push(card);
      isCardPlayed[i][j] = false;
    }
  }
  const flags = [] as FlagInhabitants[];
  const flagStatuses = [] as FlagStatus[];
  for (let i = 0; i < gameSettings.numberOfFlags; i++) {
    flags.push([[], []]);
    flagStatuses[i] = null;
  }
  const hands = [[], []] as [ICard[], ICard[]];
  const G = { remainingCards, isCardPlayed, hands, flags, flagStatuses };
  return G;
};

const hostHand = [
  cardConstructor(4, 1),
  cardConstructor(4, 7),
  cardConstructor(3, 3)
];
const tripsHand = [
  cardConstructor(3, 9),
  cardConstructor(2, 9),
  cardConstructor(1, 9)
];

const straightFlushHand = [
  cardConstructor(4, 5),
  cardConstructor(4, 6),
  cardConstructor(4, 7)
];

const flushHand = [
  cardConstructor(3, 1),
  cardConstructor(3, 8),
  cardConstructor(3, 3)
];

const handType = {
  strength: "straight",
  values: [3, 2, 1]
};

const straightHand = [
  cardConstructor(4, 1),
  cardConstructor(5, 3),
  cardConstructor(0, 2)
];

describe("determineHandType", () => {
  test("Determines Trips correctly", () => {
    const handType = {
      strength: "trips",
      values: [9, 9, 9]
    };

    expect(H.determineHandType(tripsHand)).toEqual(handType);
  });

  test("Determines straightFlush correctly", () => {
    const handType = {
      strength: "straightFlush",
      values: [7, 6, 5]
    };

    expect(H.determineHandType(straightFlushHand)).toEqual(handType);
  });

  test("Determines straight correctly", () => {
    const straightHand = [
      cardConstructor(4, 1),
      cardConstructor(5, 2),
      cardConstructor(0, 3)
    ];

    expect(H.determineHandType(straightHand)).toEqual(handType);
  });

  test("Determines flush correctly", () => {
    const handType = {
      strength: "flush",
      values: [8, 3, 1]
    };

    expect(H.determineHandType(flushHand)).toEqual(handType);
  });

  test("Determines straight correctly", () => {
    const handType = {
      strength: "straight",
      values: [3, 2, 1]
    };

    expect(H.determineHandType(straightHand)).toEqual(handType);
  });

  test("Determines host correctly", () => {
    const handType = {
      strength: "host",
      values: [7, 3, 1]
    };

    expect(H.determineHandType(hostHand)).toEqual(handType);
  });
});

describe("bestHand_2 when no cards have been played", () => {
  const G = setup();
  const potentialTrips = [cardConstructor(3, 9), cardConstructor(2, 9)];

  const potentialStraightFlush = [cardConstructor(4, 5), cardConstructor(4, 6)];

  const potentialFlush = [cardConstructor(4, 5), cardConstructor(4, 8)];

  const potentialStraight = [cardConstructor(4, 2), cardConstructor(5, 3)];

  test("potentialTrips", () => {
    expect(H.getBestHand_2(potentialTrips, G.isCardPlayed)).toEqual({
      strength: "trips",
      values: [9, 9, 9]
    });
  });
  test("potentialFlush", () => {
    expect(H.getHandBounds(potentialFlush, G.isCardPlayed)).toEqual({
      upper: {
        strength: "flush",
        values: [9, 8, 5]
      },
      lower: {
        strength: "host",
        values: [8, 5, 0]
      }
    });
  });

  test("potentialStraight", () => {
    expect(H.getHandBounds(potentialStraight, G.isCardPlayed)).toEqual({
      upper: {
        strength: "straight",
        values: [4, 3, 2]
      },
      lower: {
        strength: "host",
        values: [3, 2, 0]
      }
    });
  });
  test("potentialStraightFlush", () => {
    expect(H.getHandBounds(potentialStraightFlush, G.isCardPlayed)).toEqual({
      upper: {
        strength: "straightFlush",
        values: [7, 6, 5]
      },
      lower: {
        strength: "host",
        values: [6, 5, 0]
      }
    });
  });
});

describe("bestHand_1 when no cards have been played", () => {
  const G = setup();

  test("3, 5", () => {
    expect(H.getHandBounds([cardConstructor(3, 5)], G.isCardPlayed)).toEqual({
      upper: {
        strength: "straightFlush",
        values: [7, 6, 5]
      },
      lower: {
        strength: "host",
        values: [5, 0, 0]
      }
    });
  });

  test("3, 0", () => {
    expect(H.getHandBounds([cardConstructor(3, 0)], G.isCardPlayed)).toEqual({
      upper: {
        strength: "straightFlush",
        values: [2, 1, 0]
      },
      lower: {
        strength: "host",
        values: [0, 0, 0]
      }
    });
  });

  const G2 = setup();
  for (let suit = 0; suit < gameSettings.numberOfSuits; suit++) {
    G2.isCardPlayed[suit][9] = true;
    G2.isCardPlayed[suit][7] = true;
    G2.isCardPlayed[suit][6] = true;
    G2.isCardPlayed[suit][4] = true;
    G2.isCardPlayed[suit][2] = true;
    G2.isCardPlayed[suit][1] = true;
  }

  test("No Straights", () => {
    expect(H.getHandBounds([cardConstructor(3, 5)], G2.isCardPlayed)).toEqual({
      upper: {
        strength: "trips",
        values: [5, 5, 5]
      },
      lower: {
        strength: "host",
        values: [5, 0, 0]
      }
    });
  });

  test("No Straights, No Trips", () => {
    expect(H.getHandBounds([cardConstructor(3, 6)], G2.isCardPlayed)).toEqual({
      upper: {
        strength: "flush",
        values: [8, 6, 5]
      },
      lower: {
        strength: "host",
        values: [6, 0, 0]
      }
    });
  });
});

describe("bestHand_0", () => {
  const G = setup();

  test("emptyFlag", () => {
    expect(H.getHandBounds([], G.isCardPlayed)).toEqual({
      upper: {
        strength: "straightFlush",
        values: [9, 8, 7]
      },
      lower: {
        strength: "host",
        values: [0, 0, 0]
      }
    });
  });
  // Remove all the straights
  const G3 = setup();
  for (let suit = 0; suit < gameSettings.numberOfSuits; suit++) {
    G3.isCardPlayed[suit][9] = true;
    G3.isCardPlayed[suit][7] = true;
    G3.isCardPlayed[suit][5] = true;
    G3.isCardPlayed[suit][3] = true;
    G3.isCardPlayed[suit][1] = true;
  }
  test("No straights", () => {
    expect(H.getHandBounds([], G3.isCardPlayed)).toEqual({
      upper: {
        strength: "trips",
        values: [8, 8, 8]
      },
      lower: {
        strength: "host",
        values: [0, 0, 0]
      }
    });
  });

  //Straight flush shouldn't be possible if certain cards have been played
  const G4 = setup();
  G4.isCardPlayed[3][2] = true;
  G4.isCardPlayed[3][1] = true;

  test("3, 0", () => {
    expect(H.getHandBounds([cardConstructor(3, 0)], G4.isCardPlayed)).toEqual({
      upper: {
        strength: "trips",
        values: [0, 0, 0]
      },
      lower: {
        strength: "host",
        values: [0, 0, 0]
      }
    });
  });

  const G5 = setup();
  G5.isCardPlayed[3][1] = true;

  test("3, 0", () => {
    expect(
      H.getHandBounds(
        [cardConstructor(3, 0), cardConstructor(3, 2)],
        G4.isCardPlayed
      )
    ).toEqual({
      upper: {
        strength: "flush",
        values: [9, 2, 0]
      },
      lower: {
        strength: "host",
        values: [2, 0, 0]
      }
    });
  });

  // Straights shouldn't be possible if missing cards
  const G6 = setup();
  for (const suit of [0, 1, 2, 3, 4, 5]) {
    G6.isCardPlayed[suit][1] = true;
  }
  for (const val of [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]) {
    G6.isCardPlayed[3][val] = true;
  }
  test("3, 0", () => {
    expect(
      H.getHandBounds(
        [cardConstructor(3, 0), cardConstructor(3, 2)],
        G6.isCardPlayed
      )
    ).toEqual({
      upper: {
        strength: "host",
        values: [9, 2, 0]
      },
      lower: {
        strength: "host",
        values: [2, 0, 0]
      }
    });
  });
});

//

describe("Compare hand strengths", () => {
  test("Flush to host", () => {
    const hand1 = [0, 1, 2].map(i => cardConstructor(0, i));
    const hand2 = [0, 5, 7].map(i => cardConstructor(i, i));
    const strength1 = H.determineHandType(hand1);
    const strength2 = H.determineHandType(hand2);
    expect(H.compareHandStrength(strength1, strength2)).toEqual(1);
  });

  describe("Regression testing", () => {
    test("234suited  vs 67suited", () => {
      // Bug: When checking for straight, we didn't check both ends
      // Shouldn't use elseif.

      const isCardPlayed = {} as IsCardPlayed;
      const unPlayedCards = [
        [4, 0],
        [0, 1],
        [6, 0],
        [0, 2],
        [5, 3],
        [4, 4]
      ].concat([
        [9, 4],
        [6, 1],
        [8, 1],
        [3, 3],
        [4, 3]
      ]);
      const hand1 = listOfTuplesToCard([
        [1, 0],
        [2, 0],
        [3, 0]
      ]);
      const hand2 = listOfTuplesToCard([
        [6, 4],
        [5, 4]
      ]);

      // Mock iscardPlayed
      for (let suit = 0; suit < gameSettings.numberOfSuits; suit++) {
        isCardPlayed[suit] = {} as { [key: string]: boolean };
        for (let value = 0; value < gameSettings.numberOfCardsInSuit; value++) {
          isCardPlayed[suit][value] = true;
        }
      }

      for (let idx = 0; idx < unPlayedCards.length; idx++) {
        const [value, suit] = unPlayedCards[idx];
        isCardPlayed[suit][value] = false;
      }

      expect(H.getHandBounds(hand1, isCardPlayed)).toEqual({
        upper: {
          strength: "straightFlush",
          values: [3, 2, 1]
        },
        lower: {
          strength: "straightFlush",
          values: [3, 2, 1]
        }
      });

      expect(H.getHandBounds(hand2, isCardPlayed)).toEqual({
        upper: {
          strength: "straightFlush",
          values: [6, 5, 4]
        },
        lower: {
          strength: "host",
          values: [6, 5, 0]
        }
      });
    });
  });
});
