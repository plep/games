import React from "react";
import { IBoardProps } from "./gameTypes";
import { ICard, FlagInhabitants, FlagStatus } from "./gameTypes";
import styles from "./board.module.css";
import { ScaleToFit } from "../../utils/scaleToFit";
import { ScreenSizes } from "../../screenSizes";

interface BoardState {
  helpDrawerVisible: boolean;
  handIdx: number | null;
  flagIdx: number | null;
}

type OnClickFlag = (flagIdx: number) => void;
type OnClickCard = (handIdx: number) => void;

type SuitColor = "green" | "blue" | "pink" | "purple" | "red" | "black";

function suitToColor(suit: number): SuitColor {
  return [
    "green" as const,
    "blue" as const,
    "pink" as const,
    "purple" as const,
    "red" as const,
    "black" as const
  ][suit];
}

export class Board extends React.Component<IBoardProps, {}> {
  state = {
    helpDrawerVisible: false,
    handIdx: null,
    flagIdx: null
  };

  onClickCard = (handIdx: number) => {
    if (this.isActive()) {
      this.setState({ handIdx });
    } else {
      console.log("It's not your turn!");
    }
  };

  onClickFlag = (flagIdx: number) => {
    if (this.state.handIdx !== null) {
      this.props.moves.placeCard(this.state.handIdx, flagIdx);
      this.setState({
        handIdx: null,
        flagIdx: null
      });
    }
  };

  isActive = () => "" + this.props.ctx.currentPlayer === this.props.playerID;

  render() {

    return (
      <ScaleToFit
        childDimensions={orientation => ScreenSizes["landscape"]}
        direction={"auto"}
      >
        <div className={styles.gameUI}>
          <Cards
            hand={this.props.G.hand}
            onClickCard={this.onClickCard}
            isActive={this.isActive()}
            handIdx={this.state.handIdx}
            flagIdx={this.state.flagIdx}
          />
          <Flags
            flags={this.props.G.flags}
            flagStatuses={this.props.G.flagStatuses}
            onClickFlag={this.onClickFlag}
            isActive={this.isActive()}
            handIdx={this.state.handIdx}
            flagIdx={this.state.flagIdx}
            thisPlayer={parseInt(this.props.playerID)}
          />
        </div>
      </ScaleToFit>
    );
  }
}

interface ICardsProps {
  hand: ICard[];
  onClickCard: OnClickCard;
  isActive: boolean;
  handIdx: number | null;
  flagIdx: number | null;
}

function Cards({ hand, onClickCard, isActive, handIdx, flagIdx }: ICardsProps) {
  const classes = [styles.handDisplay];
  if (isActive && handIdx === null) {
    classes.push(styles.needsInput);
  }
  return (
    <div className={classes.join(" ")}>
      {hand.map((card: ICard, cardHandIdx: number) => {
        return (
          card && <Card
            key={card.id}
            card={card}
            onClickCard={() => onClickCard(cardHandIdx)}
            isChosen={handIdx === cardHandIdx}
          />
        );
      })}
    </div>
  );
}

function Card({
  card,
  onClickCard,
  isChosen
}: {
  card: ICard;
  onClickCard: () => void;
  isChosen: boolean;
}) {
  const classes = [styles.card, styles.cardDisplay];
  if (isChosen) {
    classes.push(styles.chosenInput);
  }
  classes.push(styles[suitToColor(card.suit)]);
  return (
    <button className={classes.join(" ")} onClick={onClickCard}>
      {card.value + 1}
    </button>
  );
}

function FlagCard({ card }: { card: ICard | null }) {
  const classes = [styles.card, styles.flagCard];
  if (card) {
    classes.push(styles[suitToColor(card.suit)]);
  }
  return card ? (
    <div className={classes.join(" ")}>{card.value + 1}</div>
  ) : (
    <div className={classes.join(" ") + " " + styles.empty} />
  );
}

interface IFlagsProps {
  flags: FlagInhabitants[];
  flagStatuses: FlagStatus[];
  onClickFlag: OnClickFlag;
  isActive: boolean;
  handIdx: number | null;
  flagIdx: number | null;
  thisPlayer: number;
}

function Flags({
  flags,
  flagStatuses,
  onClickFlag,
  isActive,
  handIdx,
  flagIdx,
  thisPlayer
}: IFlagsProps) {
  const classes = [styles.flagsDisplay];
  if (isActive && handIdx !== null && flagIdx === null) {
    classes.push(styles.needsInput);
  }
  return (
    <div className={classes.join(" ")}>
      {flags.map((flagInhabitants: FlagInhabitants, idx: number) => (
        <Flag
          key={idx}
          flagInhabitants={flagInhabitants}
          onClickFlag={() => onClickFlag(idx)}
          thisPlayer={thisPlayer}
          flagStatus={flagStatuses[idx]}
        />
      ))}
    </div>
  );
}

function Flag({
  flagInhabitants,
  flagStatus,
  onClickFlag,
  thisPlayer
}: {
  flagInhabitants: FlagInhabitants;
  flagStatus: FlagStatus;
  onClickFlag: () => void;
  thisPlayer: number;
}) {
  const otherPlayer = thisPlayer === 1 ? 0 : 1;
  const status =
    flagStatus === null ? null : flagStatus === thisPlayer ? "ours" : "theirs";
  return (
    <div className={styles.flag} onClick={onClickFlag}>
      <FlagHand hand={flagInhabitants[otherPlayer]} reverse={true} />
      <FlagSymbol status={status} />
      <FlagHand hand={flagInhabitants[thisPlayer]} reverse={false} />
    </div>
  );
}

function FlagHand({ hand, reverse }: { hand: ICard[]; reverse: boolean }) {
  const maxChildren = 3;
  const sortedHand = hand.slice() as any;
  for (let i = 0; i < maxChildren - hand.length; i++) {
    sortedHand.push(null);
  }
  const classes = [styles.flagHand];
  if (reverse) {
    classes.push(styles.flagHandOther);
  }
  return (
    <div className={classes.join(" ")}>
      {sortedHand.map((card: ICard | null, idx: number) => (
        <FlagCard key={idx} card={card} />
      ))}
    </div>
  );
}

function FlagSymbol({ status }: { status: "theirs" | "ours" | null }) {
  const classes = [styles.flagSymbol];
  if (status !== null) {
    classes.push(styles[status]);
  }
  return <div className={classes.join(" ")} />;
}
