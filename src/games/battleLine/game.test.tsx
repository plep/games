import { flagResolver, cardConstructor } from "./game";

import {
  IGameState,
  ICard,
  IGameCtx,
  FlagInhabitants,
  IsCardPlayed,
  CardValue,
  HandStrength,
  FlagBound,
  FlagStatus
} from "./gameTypes";

const gameSettings = {
  numberOfCardsInSuit: 10,
  numberOfSuits: 6,
  numberOfFlags: 9,
  handSize: 7
};
const setup = () => {
  const remainingCards = [];
  const isCardPlayed = {} as IsCardPlayed;
  for (let i = 0; i < gameSettings.numberOfSuits; i++) {
    isCardPlayed[i] = {} as { [key in CardValue]: boolean };
    for (let j = 0; j < gameSettings.numberOfCardsInSuit; j++) {
      const card = cardConstructor(i, j);
      remainingCards.push(card);
      isCardPlayed[i][j] = false;
    }
  }
  const flags = [] as FlagInhabitants[];
  const flagStatuses = [] as FlagStatus[];
  for (let i = 0; i < gameSettings.numberOfFlags; i++) {
    flags.push([[], []]);
    flagStatuses[i] = null;
  }
  const hands = [[], []] as [ICard[], ICard[]];
  const G = { remainingCards, isCardPlayed, hands, flags, flagStatuses };
  return G;
};

describe("Flag Resolver", () => {
  const G = setup();
  test("No cards", () => {
    expect(flagResolver([[], []], G.isCardPlayed)).toEqual(null);
  });
});

const DidPlayerWin = (id: 0 | 1, flagStatuses: any): boolean => {
  let totalFlags = 0;
  let threeInARow = 0;
  // TODO: make gamesettings
  for (let i = 0; i < gameSettings.numberOfFlags; i++) {
    if (flagStatuses[i] === id) {
      totalFlags += 1;
      threeInARow += 1;
    } else {
      threeInARow = 0;
    }
    if (threeInARow === 3 || totalFlags >= 5) {
      return true;
    }
  }
  return false;
};

describe("WinCheck", () => {
  const G2 = setup();
  G2.flagStatuses = [null, null, null, null, null, null, 0, 0, 0, null];
  test("3 in a row victory ", () => {
    expect(DidPlayerWin(0, G2.flagStatuses)).toEqual(true);
  });
});
