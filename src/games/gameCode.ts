import { BattleLineObj } from "./battleLine/game";
import { CodeNamesDuet as CodeNamesDuetBase } from "./codenamesDuet/game";
import { Game } from "./ticTacToe/game";
import { createLanVersion } from "./gameGenerator";

export const BattleLineLan = createLanVersion(BattleLineObj as any);
export const TicTacToeLan = createLanVersion(Game);
export const CodeNamesDuet = createLanVersion(CodeNamesDuetBase);

export const newGames = [BattleLineLan, TicTacToeLan, CodeNamesDuet];
