import { createLanBoard } from "./boardGenerator";

//BattleLine and codenamesDuet
import { BattleLineLan, CodeNamesDuet as CodeNamesDuetLan } from "./gameCode";
import { Board as BattleLineBoard } from "./battleLine/board";
import { Board as CodeNamesDuetBoard } from "./codenamesDuet/board";
// TicTacToe
import { TicTacToeLan } from "./gameCode";
import { Board as TicTacToeBoard } from "./ticTacToe/board";

// Other games
import { CodeNames } from "./codenames/gameComponent";
import { GameComponent } from "../lobby/lobbyTypes";

const BattleLineLanBoard = createLanBoard(BattleLineBoard);
export const BattleLine = {
  game: BattleLineLan,
  board: BattleLineLanBoard,
  description:
    "A streamlined two player bluffing game designed by Reiner Knizia.",
};

const TicTacToeLanBoard = createLanBoard(TicTacToeBoard);
export const TicTacToe = {
  game: TicTacToeLan,
  board: TicTacToeLanBoard,
  description: "Also known as noughts and crosses.",
};

const CodeNamesDuetLanBoard = createLanBoard(CodeNamesDuetBoard);
export const CodeNamesDuet = {
  game: CodeNamesDuetLan,
  board: CodeNamesDuetLanBoard,
  description: "A two player co-op version of Codenames.",
};

export const games: { [key: string]: GameComponent } = {
  CodeNames,
  CodeNamesDuet,
  BattleLine,
  TicTacToe,
};
