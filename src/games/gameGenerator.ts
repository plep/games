import { Game } from "boardgame.io/core";
import { IGameState, Lan, GameCtx, GameObject, PlayerId } from "./gameGenTypes";
// Takes a game object and outputs enhanced versions:
// PnP hotseat play
// Host + Controller play
// Hotseat + Secret Info play
// `Lan play' -- Needs setup screen
// Online play

export function createLanVersion<GameState extends IGameState, StripGameState>(
  game: GameObject<GameState, StripGameState>
) {
  // Need to be able to re-arrange players
  // Extra state: order
  // Extra move: re-arrange order]
  const setup = () => {
    console.log(game.flow);
    const order = Array.from(Array(game.maxPlayers), (x, idx) => idx);
    return { order, ...game.setup() };
  };

  const playerView = (G: Lan<GameState>, ctx: GameCtx, playerId: string) => {
    if (game.playerView !== undefined) {
      return { order: G.order, ...game.playerView(G, ctx, playerId) };
    } else {
      return { ...G }; // For some reason playerView is very buggy
    }
  };

  const newMoves = {
    updateOrder(G: Lan<GameState>, ctx: GameCtx, newArray: PlayerId[]) {
      // Whenever the host re-arranges players in the lobby
      G.order = newArray;
    },

    startGame(G: Lan<GameState>, ctx: GameCtx) {
      console.log("Starting game...");
      if (game.initGameState !== undefined) {
        game.initGameState(G, ctx);
      } else {
        console.log("Warning, initGameState not defined");
      }

      const newPlayers = {} as { [key: number]: number };
      if (G.players !== undefined) {
        for (let playerId in G.players) {
          if (G.players.hasOwnProperty(playerId)) {
            newPlayers[G.order[playerId]] = G.players[playerId];
          }
        }
      }
      ctx.events.endTurn();
      ctx.events.endPhase();
      console.log(`Active players are ${ctx.actionPlayers}`);
    },
  };

  const customTO = {
    playOrder: (G: Lan<GameState>) => G.order,
    first: (G: Lan<GameState>, ctx: GameCtx) => 0,
    next: (G: Lan<GameState>, ctx: GameCtx) =>
      (ctx.playOrderPos + 1) % G.order.length,
  };
  let flow;
  if (game.flow !== undefined) {
    flow = {
      ...game.flow,
      turnOrder: customTO,
      startingPhase: "gameSetup",
      phases: {
        ...game.flow.phases,
        gameSetup: {
          allowedMoves: ["startGame", "updateOrder"],
          next:
            game.flow.startingPhase !== undefined
              ? game.flow.startingPhase
              : "default",
        },
      },
    };
  } else {
    flow = {
      turnOrder: customTO,
      startingPhase: "gameSetup",
      phases: {
        gamePlay: {},
        gameSetup: {
          allowedMoves: ["startGame", "updateOrder"],
          next: "default",
        },
      },
    };
  }

  return Game({
    ...game,
    flow,
    setup,
    playerView,
    moves: { ...game.moves, ...newMoves },
  });
}
