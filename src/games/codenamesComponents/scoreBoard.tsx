import React from "react";
import styles from "./scoreBoard.module.css";
import { capitalize } from "../../utils/capitalize";

export function ScoreBoard(props: {
  active_team: string;
  cards: any[];
  winConditions: { red: number; blue: number };
  gameover: { winner?: string };
}) {
  const calculate_score = (cards: any) => {
    // for performance, should write as update_score
    // assassinated is blue or red if they uncovered the assassin.
    const out = { blue: 0, red: 0, assassinated: null };
    for (let i = 0; i < cards.length; i++) {
      const card = cards[i];
      if (card.who_revealed) {
        if (card.team === "blue") {
          out.blue += 1;
        } else if (card.team === "red") {
          out.red += 1;
        } else if (card.team === "assassin" && !out.assassinated) {
          out.assassinated = card.who_revealed;
        }
      }
    }
    return out;
  };
  const score = calculate_score(props.cards);

  const teamHeader = (team: "red" | "blue") => {
    const classes = [styles.scoreBoardHeader];
    if (props.active_team === team && !props.gameover) {
      classes.push(styles.activeText);
    }
    return (
      <>
        <span className={classes.join(" ")}>
          {/* {props.active_team === team && "<"} */}
          {capitalize(team)}
          {/* {props.active_team === team && ">"} */}
        </span>
      </>
    );
  };
  return (
    <div className={styles.ScoreCard}>
      <table style={{ textAlign: "center" }} className={styles.ScoreBoardTable}>
        <tbody>
          <tr>
            <td>
              {ScoreCounter(
                score["blue"],
                props.winConditions["blue"],
                styles.blue
              )}
            </td>
            <td>
              {ScoreCounter(
                score["red"],
                props.winConditions["red"],
                styles.red
              )}
            </td>
          </tr>
        </tbody>
        <thead>
          <tr>
            <td>{teamHeader("blue")}</td>
            <td>{teamHeader("red")}</td>
          </tr>
        </thead>
      </table>
    </div>
  );
}

export function CoopScoreBoard(props: {
  cards: any[];
  winConditions: { green: number; white: number };
  score: number;
  assassinated: boolean;
  turnsRemaining: number;
  gameover: { winner?: "players" | null };
}) {
  const classes = [];
  if (props.gameover && props.gameover.winner === null) {
    classes.push(styles.fade);
  }
  return (
    <div className={styles.CoopScoreCard}>
      {ScoreCounter(props.score, props.winConditions["green"], styles.green)}
      {ScoreCounter(
        props.winConditions["white"] - props.turnsRemaining,
        props.winConditions["white"],
        styles.white
      )}
    </div>
  );
}

function ScoreCounter(
  score: number,
  maximum: number,
  color: string // css className
) {
  if (score > maximum) {
    throw new Error(
      `score (${score}) cannot be greater than maximum (${maximum})`
    );
  }

  return (
    // Without the key, the transitions won't work as the nodes will be
    // destroyed and recreated when the className changes.
    <div className={styles.scoreIndicatorRow}>
      {new Array(maximum)
        .fill(0)
        .map((x, idx) =>
          idx < score ? (
            <div key={idx} className={styles.scoreIndicator + " " + color} />
          ) : (
            <div key={idx} className={styles.scoreIndicator} />
          )
        )}
    </div>
  );
}
