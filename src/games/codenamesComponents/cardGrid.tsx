import React from "react";
import styles from "./codenames.module.css";

interface CardGridProps {
  cards: any[];
  cardComponent: any;
  onClick: (id: number) => void;
  player_role?: number;
  indicatorColor?: (card: any) => string;
  passButton?: JSX.Element;
  showIndicator: boolean;
  roleName: "spectator" | "spymaster" | "spyslave";
  helpButton?: JSX.Element;
}

export function CardGrid(props: CardGridProps) {
  const CardBox = props.cardComponent;
  return (
    <div className={styles.grid} id="cardGrid">
      {props.cards.map((card, idx) => (
        <CardBox
          key={idx}
          tabIndex={idx}
          card={card}
          indicatorColor={
            props.indicatorColor ? props.indicatorColor(card) : null
          }
          onClick={() => props.onClick(card.id)}
          showIndicator={props.showIndicator}
          roleName={props.roleName}
          player_role={props.player_role}
        />
      ))}

      {props.passButton && (
        <div
          tabIndex={101}
          key={"passButton"}
          className={styles.PassButtonWrapper}
        >
          {props.passButton}
        </div>
      )}
      {props.helpButton && (
        <div
          tabIndex={102}
          key={"helpButton"}
          className={styles.HelpButtonWrapper}
        >
          {props.helpButton}
        </div>
      )}
    </div>
  );
}
