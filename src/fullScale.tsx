import React from "react";
import { Dimensions } from "./screenSizes";

interface ScaledComponentProps {
  enabled: boolean;
}

export const fullScale = <P extends object>(
  Component: React.ComponentType<P>,
  screenSize: Dimensions
) => {
  class ScaledComponent extends React.Component<P & ScaledComponentProps> {
    componentDidMount() {
      window.addEventListener("resize", () => this.forceUpdate());
    }

    componentWillUnmount() {
      window.removeEventListener("resize", () => this.forceUpdate());
    }
    get transformInfo() {
      const windowWidth = window.innerWidth;
      const windowHeight = window.innerHeight;

      const viewRatio = windowWidth / windowHeight;
      const contentRatio = screenSize.width / screenSize.height;

      let scaleFactor;
      let y_translate;
      let x_translate;
      let final_height;
      let final_width;

      if (viewRatio > contentRatio) {
        scaleFactor = windowHeight / screenSize.height;
        final_width = windowHeight * contentRatio;
        // Equal space on left/right
        x_translate = (windowWidth - final_width) / 2;
        y_translate = 0;
      } else if (viewRatio <= contentRatio) {
        scaleFactor = windowWidth / screenSize.width;
        final_height = windowWidth / contentRatio;
        // Equal space on top/bottom
        x_translate = 0;
        y_translate = (windowHeight - final_height) / 2;
      }
      return {
        scaleFactor,
        y_translate,
        x_translate,
        final_width,
        final_height
      };
    }
    render() {
      const VISIBLE = "visible" as const;
      const HIDDEN = "hidden" as const;
      const windowWidth = window.innerWidth;
      const windowHeight = window.innerHeight;
      const tInfo = this.transformInfo;
      const transformStyle = `translateY(${tInfo.y_translate}px) translateX(${
        tInfo.x_translate
      }px) scale(${tInfo.scaleFactor})`;
      const wrapStyle = {
        transform: transformStyle,
        transformOrigin: "0 0",
        maxHeight: windowHeight,
        maxWidth: windowWidth,
        opacity: this.props.enabled ? 1 : 0,
        visibility: this.props.enabled ? VISIBLE : HIDDEN
      };
      return (
        <div style={wrapStyle}>
          <Component {...this.props} />
        </div>
      );
    }
  }
  return ScaledComponent;
};
