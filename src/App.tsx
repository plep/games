import React from "react";
import { Client } from "boardgame.io/react";
import Lobby from "./lobby/lobby";
import { Config } from "./config";
import {  games } from "./games/gameComponents";
import {
  HashRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";
import "./App.css";
import { ScreenLabel, RoutePassProps } from "./lobby/lobbyTypes";

const serverURL = Config.serverURL;
console.log("Server URL: ", serverURL);

export default class App extends React.Component<{}, { debugMode: boolean }> {
  render() {
    const LobbyComponentFactory = (screenLabel: ScreenLabel) => {
      const LobbyComponent = (props: RoutePassProps) => {
        return (
          <Lobby
            defaultGame={"CodeNamesDuet"}
            gameComponents={games}
            gameServer={serverURL}
            lobbyServer={serverURL}
            clientFactory={Client}
            debug={true}
            currentScreen={screenLabel}
            {...props}
          />
        );
      };
      return LobbyComponent;
    };
    return (
      <>
        <Router>
          <Switch>
            <Route
              exact
              path="/login"
              render={LobbyComponentFactory("login")}
            />
            <Route
              exact
              path="/login"
              render={LobbyComponentFactory("login")}
            />
            <Route
              exact
              path="/create"
              render={LobbyComponentFactory("home")}
            />
            <Route
              exact
              path="/play/:gameId"
              render={LobbyComponentFactory("play")}
            />
            <Redirect to={"/login"} />
          </Switch>
        </Router>
      </>
    );
  }
}
