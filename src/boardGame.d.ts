export interface GameComponent {
  game: {
    name: string;
    maxPlayers: number;
    minPlayers: number;
    displayName: string;
  };
  board: any;
}

export interface GameState {
  order: number[];
  players: any[];
  [key: string]: any;
}

export interface GameContext {
  phase: string;
  gameover: {
    winner: any;
  };
  currentPlayer: string;
}

export interface BoardProps {
  G: GameState;
  playerID: string;
  ctx: GameContext;
  gameID: string;
  moves: any;
  isActive: boolean;
}
